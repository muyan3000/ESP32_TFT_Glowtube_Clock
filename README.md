# TFT仿辉光管天气时钟

#### 介绍
ESP32使用TFT屏幕的仿辉光管天气时钟

程序功能参考多个天气时钟作品，仅为可用


![输入图片说明](images/%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20230728194509.jpg)![输入图片说明](images/%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20230627214949.jpg)

长按小按钮开机


程序编译前需要修改

esp32_tft_clock.ino中wifi配置信息


```
const char *ssid = "wifi";                // Wifi名称
const char *password = "123456789"; // Wifi密码
```

weather.h中天气的配置信息


```
const String UserKey = ""; // 私钥获取网址https://dev.heweather.com/docs/start/get-api-key

String city_ID_str="101020200"; // 城市id信息
```

