
/******************************************
 * Function name : setRtcTime
 * Return : void ;
 * @ s : 秒
 * @ m : 分
 * @ h : 时
 * @ w : 星期 (星期天为0x00)
 * @ d : 日
 * @ mh : 月
 * @ y : 年 (20xx) 如2022年 就是 22
 ********************************************/
// rtc.setRtcTime(0, 24, 21, 0 , 4, 4, 22);

/*
读取时钟数据是否正常
*/
boolean ClockReadCheck()
{
  if (rtc.getYear() < 20 || rtc.getYear() > 99)
    return 0;
  else if (rtc.getMonth() <= 0 || rtc.getMonth() > 12)
    return 0;
  else if (rtc.getDate() <= 0 || rtc.getDate() > 31)
    return 0;
  else if (rtc.getHour() < 0 || rtc.getHour() > 24)
    return 0;
  else if (rtc.getMinute() < 0 || rtc.getMinute() > 60)
    return 0;
  else if (rtc.getSecond() < 0 || rtc.getSecond() > 60)
    return 0;
  else
    return 1;
}

// 时间同步，NTP更新
void timeUpdate(bool update)
{
  if (ClockReadCheck() && update != true) //检测RX8025内数据是否正常，有正常数据则更新系统时间戳，否则连接wifi从NTP update
  {
    setTime(rtc.getUnixtime()+28801); //+8*60*60
    Serial.println("setTime OK");
  }
  else
  {
    if (WiFi.status() != WL_CONNECTED)
    {
      // 连接WiFi
      if (!AutoWifiConfig())
      {
        SmartConfig();
      }
    }

    if (WiFi.status() == WL_CONNECTED)
    {
      // NTP更新
      timeClient.update();
      Serial.println("NTP Update OK");
    }

    // ntp更新成功则更新系统时间
    if (timeClient.isTimeSet())
    {
      setTime(timeClient.getEpochTime());
      Serial.print("NTP setTime OK Unixtime:");
      Serial.println(timeClient.getEpochTime());
      rtc.setRtcTime(second(), minute(), hour(), (weekday() - 1), day(), month(), year() - 2000);
      Serial.print("setRtcTime OK Unixtime:");
      Serial.println(rtc.getUnixtime());
      // Serial.println(year());
    }
    else
    { // ntp更新失败则从rtc获取
      Serial.println("NTP Update Faild");
      Serial.print("Rtc Unixtime:");
      Serial.println(rtc.getUnixtime()+28801);
      if(ClockReadCheck())
        setTime(rtc.getUnixtime()+28801);
    }
  }
}
