#include <Wire.h>
#include "ClosedCube_SHT31D.h"

ClosedCube_SHT31D sht3xd;

#include <TimeLib.h>
#include "RX8025.h"
RX8025 rtc;

unsigned char tempDay, tempHour, tempMinute, tempSecond; // 星期，时，分，秒
int tempYear, tempMonth, tempDate;                       // 年，月，日

unsigned int icon;   //天气图标

#include <WiFi.h>
#include <WiFiUdp.h>
#include <NTPClient.h>
#include <HTTPClient.h>
#include <ArduinoOTA.h>
#include <WebServer.h>
#include <ArduinoJson.h>
#include <lvgl.h>
#include "weather.h"

void updateUi(int allupdate = 0);
void timeUpdate(bool update = 0);

unsigned char humidity; // 湿度
unsigned char Uv;       // 紫外线强度
int wetemp = 0;         // 温度

// WebServer ESP32Server(80);

const char *username = "admin";                  // web用户名
const char *userpassword = "123456789"; // web用户密码

const char *ssid = "wifi";                // Wifi名称
const char *password = "123456789"; // Wifi密码


// 使用Wi-Fi时不能使用ADC2管脚ADC2_CH0 GPIO 4 0 2 15 13 12 14 27 25 26
#define INPUT_VOLTAGE_SENSE_PIN 39 // 环境光检测
#define IO_ON_OFF GPIO_NUM_25      // 电池开关
#define RADAR_PIN GPIO_NUM_34      // 雷达波人体检测
#define bat_vcc_pin GPIO_NUM_35    // 电池电压检测

// double  R1B_VOLTAGE = 68000; //68K
// double  R2B_VOLTAGE = 10000; // 10K
double batVoltageDivRatio = 7.8;
double batVoltage;
float bat_vcc = 0.0; // 电池电压

double IlluminanceDivRatio = 7.8;
double IlluminanceADC;

int photoContent = 0; // 光照pwm

const int LED_PWM_CHANNEL = 2;       // SYSTEM PARAMETER - Channel for fan PWM
const int LED_PWM_MIN = 0;           // CALIB PARAMETER  - Minimun fan PWM (0-100). Two-wire fans do not operate below 35%
const int LED_PWM_RESOLUTION = 8;    // CALIB PARAMETER  - Fan PWM resolution. (8-15). More resolution, more slow fan response
const int LED_PWM_FRECUENCY = 16000; // CALIB PARAMETER  - Fan PWM frecuency. Change can reduce noise.

// LedPWM Init
int maxLedPwm; // Max value of PWM
int minLedPwm; // Min value of PWM
int dutyCycle = 20;

#include <FastLED.h>
#define ARRAY_SIZE(A) (sizeof(A) / sizeof((A)[0]))
#define DATA_PIN GPIO_NUM_19
// #define CLK_PIN   4
#define LED_TYPE WS2812B
#define COLOR_ORDER GRB
#define NUM_LEDS 18
#define FRAMES_PER_SECOND 120
uint8_t gHue = 0; // rotating "base color" used by many of the patterns

CRGB leds[NUM_LEDS];
int rgb_brightness = 25; // 初始化亮度

float Temperature, Humidityure = 0.00; //板载SHT31

const int threshold_top = 20;   // 触摸顶部阈值
const int threshold_bottom = 1; // 触摸底部阈值，越接近数值越小
const int threshold_count = 4;  // 触摸计数器有效值，通常会有意外的自动触发

int touchread[4] = {100, 100, 100, 100}; // 初始化触摸读取值为100，无触摸
int touchDetected[4] = {};               // 通过touchdetected持续计数判断是否按键，防止无触碰触发

bool touch_touched[4] = {};         // 单击判断
int touch_touched_times[4] = {};    // 单击次数，单击切换模式，双击
int touch_touching_time[4] = {};    // 持续触摸秒数，用于判断长按事件，长按关闭，长按开启，开启状态长按调光，
bool touch_STATE[4] = {1, 1, 1, 0}; // 定义按键触发对象状态变量初始值为true默认开启 0 null 1 T2 2 T3 3 T4

WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, "ntp1.aliyun.com", 8 * 3600, 60000); // udp，服务器地址，时间偏移量，更新间隔

int num_0, num_1, num_2, num_3, num_4, num_5 = 0;  //时钟数字

char DateTimeStr[20];
char weekStr[7][10] = {"日", "一", "二", "三", "四", "五", "六"};
bool num_update[6] = {1, 1, 1, 1, 1, 1};

#include <Arduino_GFX_Library.h>

#include "ui.h"

#define SCLK 18
#define MOSI 23
#define TFT_CS1 17
#define TFT_CS2 16
#define TFT_CS3 26
#define TFT_DC 33
#define TFT_RST 4

// #define GFX_BL DF_GFX_BL // default backlight pin, you may replace DF_GFX_BL to actual backlight pin
#define GFX_BL 32

int TFT_CS[6] = {17, 16, 26, 27, 14, 12};
// int TFT_CS[6] = {12, 13, 14, 17, 16, 26};

/* More dev device declaration: https://github.com/moononournation/Arduino_GFX/wiki/Dev-Device-Declaration */
#if defined(DISPLAY_DEV_KIT)
Arduino_GFX *gfx = create_default_Arduino_GFX();
#else /* !defined(DISPLAY_DEV_KIT) */

/* More data bus class: https://github.com/moononournation/Arduino_GFX/wiki/Data-Bus-Class */
// Arduino_DataBus *bus = create_default_Arduino_DataBus();
// Arduino_DataBus *bus = new Arduino_HWSPI(TFT_DC, TFT_CS, SCLK, MOSI, MISO);
Arduino_DataBus *bus = new Arduino_ESP32SPI(TFT_DC /* DC */, GFX_NOT_DEFINED /* CS */, SCLK /* SCK */, MOSI /* MOSI */, GFX_NOT_DEFINED /* MISO */, HSPI /* spi_num */);

/* More display class: https://github.com/moononournation/Arduino_GFX/wiki/Display-Class */
// Arduino_GFX *gfx = new Arduino_ILI9341(bus, DF_GFX_RST, 0 /* rotation */, false /* IPS */);
Arduino_GFX *gfx = new Arduino_ST7735(bus, TFT_RST /* RST */, 1 /* rotation */, false /* IPS */, 128 /* width */, 64 /* height */, 0 /* col offset 1 */, 67 /* row offset 1 */, 2 /* col offset 2 */, 0 /* row offset 2 */, true /* BGR */);

#endif /* !defined(DISPLAY_DEV_KIT) */
/*******************************************************************************
 * End of Arduino_GFX setting
 ******************************************************************************/

/* Change to your screen resolution */
static uint32_t screenWidth;
static uint32_t screenHeight;
static lv_disp_draw_buf_t draw_buf;
static lv_color_t *disp_draw_buf;
static lv_disp_drv_t disp_drv;

/* Display flushing */
void my_disp_flush(lv_disp_drv_t *disp, const lv_area_t *area, lv_color_t *color_p)
{
   uint32_t w = (area->x2 - area->x1 + 1);
   uint32_t h = (area->y2 - area->y1 + 1);

#if (LV_COLOR_16_SWAP != 0)
   gfx->draw16bitBeRGBBitmap(area->x1, area->y1, (uint16_t *)&color_p->full, w, h);
#else
   gfx->draw16bitRGBBitmap(area->x1, area->y1, (uint16_t *)&color_p->full, w, h);
#endif

   lv_disp_flush_ready(disp);
}

// 判断浮点数是否接近于零的函数
bool isCloseToZero(double value, double threshold)
{
   return (value > -threshold) && (value < threshold);
}

void taskWifiData()
{
   if (WiFi.status() != WL_CONNECTED)
   {
      // 连接WiFi
      if (!AutoWifiConfig())
      {
         SmartConfig();
      }
   }

   if (WiFi.status() == WL_CONNECTED)
   {
      timeUpdate(1);
      wheather_Uv_get();
      Uforecast_get();

      WiFi.disconnect();
      WiFi.mode(WIFI_OFF);
   }
}

void setup()
{
   Serial.begin(115200);
   Serial.println("Ready");

   rtc.RX8025_init();

   Serial.println(rtc.getUnixtime());
   Serial.print(rtc.getYear());
   Serial.print(" ");
   Serial.print(rtc.getMonth());
   Serial.print(" ");
   Serial.print(rtc.getDate());
   Serial.print(" ");
   Serial.print(rtc.getHour());
   Serial.print(" ");
   Serial.print(rtc.getMinute());
   Serial.print(" ");
   Serial.println(rtc.getSecond());

   //******  电池电压获取和dht30数据获取 ******

   analogSetPinAttenuation(bat_vcc_pin, ADC_6db);

   pinMode(IO_ON_OFF, OUTPUT);
   digitalWrite(IO_ON_OFF, HIGH);   //电池开关高电平保持

   for (byte i = 0; i < 6; i++)
   {
      pinMode(TFT_CS[i], OUTPUT);
      digitalWrite(TFT_CS[i], LOW);
   }

   Wire.begin();

   taskWifiData();   //连接wifi，更新时间，天气数据，连接不上则自动进入配网模式

   // while (!Serial);
   sht3xd.begin(0x44); // I2C address: 0x44 or 0x45
   Serial.print("sht3x Serial #");
   Serial.println(sht3xd.readSerialNumber());

   if (sht3xd.periodicStart(SHT3XD_REPEATABILITY_HIGH, SHT3XD_FREQUENCY_10HZ) != SHT3XD_NO_ERROR)
      Serial.println("[ERROR] Cannot start periodic mode");

   FastLED.addLeds<LED_TYPE, DATA_PIN, COLOR_ORDER>(leds, NUM_LEDS).setCorrection(TypicalLEDStrip).setDither(rgb_brightness < 255);
   // set master brightness control
   FastLED.setBrightness(rgb_brightness);
   CRGB c_rgb[4];
   c_rgb[0] = CRGB::White;
   c_rgb[1] = CRGB::Green;
   c_rgb[2] = CRGB::Red;
   c_rgb[3] = CRGB::Blue;

   for (int j = 0; j < 4; j++)
   {
      for (int i = 0; i < NUM_LEDS; i++)
      {
         leds[i] = c_rgb[j];
         FastLED.show();
         delay(15);
      }
      delay(300);
   }
   FastLED.clearData();
   FastLED.show();

   // Init Display
   gfx->begin();
   gfx->fillScreen(BLACK);

   maxLedPwm = pow(2, LED_PWM_RESOLUTION) - 1; // Get PWM Max Bit Ceiling
   minLedPwm = (LED_PWM_MIN * maxLedPwm) / 100;

   ledcSetup(LED_PWM_CHANNEL, LED_PWM_FRECUENCY, LED_PWM_RESOLUTION); // Set PWM Parameters
   ledcAttachPin(32, LED_PWM_CHANNEL);                                // Set pin as PWM
   // Send value to LED
   ledcWrite(LED_PWM_CHANNEL, dutyCycle);

   lv_init();

   screenWidth = gfx->width();
   screenHeight = gfx->height();
#ifdef ESP32
   disp_draw_buf = (lv_color_t *)heap_caps_malloc(sizeof(lv_color_t) * screenWidth * 10, MALLOC_CAP_INTERNAL | MALLOC_CAP_8BIT);
#else
   disp_draw_buf = (lv_color_t *)malloc(sizeof(lv_color_t) * screenWidth * 10);
#endif
   if (!disp_draw_buf)
   {
      Serial.println("LVGL disp_draw_buf allocate failed!");
   }
   else
   {
      lv_disp_draw_buf_init(&draw_buf, disp_draw_buf, NULL, screenWidth * 10);

      /* Initialize the display */
      lv_disp_drv_init(&disp_drv);
      /* Change the following line to your display resolution */
      disp_drv.hor_res = screenWidth;
      disp_drv.ver_res = screenHeight;
      disp_drv.flush_cb = my_disp_flush;
      disp_drv.draw_buf = &draw_buf;
      lv_disp_drv_register(&disp_drv);

      ui_init();

      /* Create simple label */
      // lv_obj_t *label = lv_label_create(lv_scr_act());
      // lv_label_set_text(label, "Hello Arduino! ");
      // lv_obj_align(label, LV_ALIGN_LEFT_MID, 0, 0);

      Serial.println("Setup done");
   }

   updateUi(1);
}

typedef void (*SimplePatternList[])();
SimplePatternList gPatterns = {rainbow, rainbowWithGlitter, confetti, sinelon, juggle, bpm, pride};

uint8_t gCurrentPatternNumber = 0; // Index number of which pattern is current

void loop()
{

   if (WiFi.status() == WL_CONNECTED)
   {
      // delay(1);//allow the cpu to switch to other tasks
      ArduinoOTA.handle();
   }
   lv_timer_handler(); /* let the GUI do its work */

   double ADCVoltage, tmpAdc, maxAdc, minAdc;

   // 触摸感应处理
   touchAttach(1, T2); // GPIO2
   touchAttach(2, T3); // GPIO15
   touchAttach(3, T4); // GPIO13

   touchedTask();

   updateUi(0);

   EVERY_N_SECONDS(1)
   {
      // 获取日期时间更新
      tempYear = year();
      tempMonth = month();
      tempDate = day();        // 日
      tempDay = weekday() - 1; // 星期
      tempHour = hour();
      tempMinute = minute();
      tempSecond = second();
      // 生成  年月日时分秒 字符串。
      Serial.printf("%d-%02d-%02d %02d:%02d:%02d\r\n", tempYear, tempMonth, tempDate, tempHour, tempMinute, tempSecond);

      // RTC获取
      Serial.print(rtc.getYear());
      Serial.print("/");
      Serial.print(rtc.getMonth());
      Serial.print("/");
      Serial.print(rtc.getDate());
      Serial.print(" ");
      Serial.print(rtc.getHour());
      Serial.print(":");
      Serial.print(rtc.getMinute());
      Serial.print(":");
      Serial.println(rtc.getSecond());
      /*
      Serial.printf("Week : %d ", rtc.getDoW());

      Serial.println(rtc.getUnixtime());
      rtc.Conversion(0, rtc.getYear(), rtc.getMonth(), rtc.getDate());
      Serial.printf("Moon: %02d%02d / %02d / %02d ", 20, 22, rtc.month_moon, rtc.day_moon);
      */

      printResult("Periodic Mode", sht3xd.periodicFetchData());

      for (int i = 0; i < 30; i++)
      {
         ADCVoltage = (analogReadMilliVolts(bat_vcc_pin) / 1000.0);
         // if(ADCVoltage>maxAdc || isCloseToZero(maxAdc, 0.001)){ maxAdc = ADCVoltage;}
         // if(ADCVoltage<minAdc || isCloseToZero(maxAdc, 0.001)){ minAdc = ADCVoltage;}
         //  batVoltage = (ADCVoltage * R1B_VOLTAGE) / R2B_VOLTAGE;
         tmpAdc = tmpAdc + ADCVoltage; // formula for calculating voltage in i.e. GND
         taskYIELD();
      }
      batVoltage = ((tmpAdc) / 30) * batVoltageDivRatio;

      photoContent = getPhData();

      ledcWrite(LED_PWM_CHANNEL, photoContent);
      FastLED.setBrightness(photoContent);
   }

   EVERY_N_MINUTES(30)
   {
      nextPattern(); // change patterns periodically
      
      taskWifiData();

      updateUi(1);
   }

}
