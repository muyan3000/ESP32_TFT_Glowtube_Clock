//触摸感应处理
void touchAttach(int touchID, uint8_t touchPin) {
  touchread[touchID] = touchRead(touchPin);
  if ( touchread[touchID] <= threshold_top && touchread[touchID] > threshold_bottom ) { //达到触发值的计数
    //delay(38);  // 0.038秒
    touchDetected[touchID]++; //持续触摸计数
    if ( (touchDetected[touchID] >= threshold_count)  ) {  //达到触发值的，灯不亮则亮灯
    }
  } else if (touchread[touchID] > threshold_top) { //无触摸处理
    if ( touchDetected[touchID] >= threshold_count ) {  //检测无触摸之前的有效计数，触摸过则标记
      touch_touched[touchID] = true;
      touch_touched_times[touchID]++;  //触摸计数+1
    }
    touch_touching_time[touchID] = 0;  //持续触摸时间清零
    touchDetected[touchID]    = 0;  //持续触摸计数清零
  }
}


// 0 null 1 2 3按键有效
void touchedTask(){
  //单击事件处理
  if (touch_touched[1]) {
    //Serial.print("\nLight1 touched ");
    //Serial.println(touch_touched_times[1]);
    nextPattern(); // change patterns periodically
    touch_touched[1] = false;
  }

  if (touch_touched[2]) {
    //Serial.print("\nLight2 touched ");
    //Serial.println(touch_touched_times[2]);
    touch_touched[2] = false;
  }

  if (touch_touched[3]) {
    //Serial.print("\nLight2 touched ");
    //Serial.println(touch_touched_times[2]);

    touch_touched[3] = false;
  }

  //长按后处理
  if ( touch_STATE[1] ) {
    RGBShow();
  } else {
    RGBTurnOff();
  }
  if ( touch_STATE[3] ) {
    //delay(1);//allow the cpu to switch to other tasks
    ArduinoOTA.handle();
  }


  EVERY_N_MILLIS(300){

    //按键2长按大于1秒处理调光，第一次按增加亮度，第二次长按减小亮度
    if ( touch_touching_time[2] > 1) {
      if ( touch_touched_times[2] == 0 || touch_touched_times[2] % 2 == 0 ) { //第0，2,4,6..次按加亮度，1,3,5...则减
        rgb_brightness = rgb_brightness + 5;
      } else {
        rgb_brightness = rgb_brightness - 5;
      }
      //Serial.println(rgb_brightness);
      FastLED.setBrightness(rgb_brightness);
    }

    if ( touch_touching_time[3] > 1) {

    }

  }

   EVERY_N_SECONDS(1)
   {
    for (byte i = 0; i < 4; i++) {
      if (touchDetected[i] > 0) { //检测到触摸中，一秒计数一次，未触摸则清零
        touch_touching_time[i]++;
        //长按事件处理
        if (touch_touching_time[i] % 2 == 0) { //按住大于2秒
          switch (i) {
            case 0:

              break;
            case 1:
              touch_STATE[i] = !touch_STATE[i]; //灯光状态反处理
              Serial.println("LIGHTS_ON/OFF");
              break;
            case 3:
              if(touch_STATE[i]==1){
                WiFi.disconnect();
                WiFi.mode(WIFI_OFF);
                Serial.println("WIFI_OFF");
              }else{
                AutoWifiConfig();
                Serial.println("WIFI_ON");
              }
              touch_STATE[i] = !touch_STATE[i]; //状态反处理
              break;
          }
        }
      }
    }
   }
}
