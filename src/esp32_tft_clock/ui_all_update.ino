void tftCS(int cs = 0)
{
    for (byte i = 0; i < 6; i++)
    {
        digitalWrite(TFT_CS[i], HIGH);
        if (i == cs) // 低电平片选
        {
            digitalWrite(TFT_CS[i], LOW);
            num_update[i] = 0;
        }
        delay(1);
        taskYIELD();
    }
}

void setScreen(int cs = 0, int num_1 = 0)
{

    tftCS(cs);
    /*
    // 时间间隔标记
    if (cs == 5 || cs == 0)
    {
        lv_obj_set_style_border_width(ui_Screen1, 0, LV_PART_MAIN | LV_STATE_DEFAULT);
    }
    else if (cs % 2 == 0)
    {
        lv_obj_set_style_border_width(ui_Screen1, 3, LV_PART_MAIN | LV_STATE_DEFAULT);
        lv_obj_set_style_border_side(ui_Screen1, LV_BORDER_SIDE_LEFT, LV_PART_MAIN | LV_STATE_DEFAULT);
    }
    else
    {
        lv_obj_set_style_border_width(ui_Screen1, 3, LV_PART_MAIN | LV_STATE_DEFAULT);
        lv_obj_set_style_border_side(ui_Screen1, LV_BORDER_SIDE_RIGHT, LV_PART_MAIN | LV_STATE_DEFAULT);
    }
    */

    // lv_obj_add_flag(ui_Label1, LV_OBJ_FLAG_HIDDEN); /// Flags
    // lv_obj_add_flag(ui_Label2, LV_OBJ_FLAG_HIDDEN); /// Flags
    lv_obj_set_align(ui_Label2, LV_ALIGN_CENTER);
    _ui_flag_modify(ui_Image99, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_ADD);

    if (cs == 0)
    {
        // lv_obj_clear_flag(ui_Label1, LV_OBJ_FLAG_HIDDEN); /// Flags
        // lv_obj_clear_flag(ui_Label2, LV_OBJ_FLAG_HIDDEN); /// Flags
        lv_label_set_text(ui_Label1, (char *)(String(day3.weather)).c_str());                                  // 预告
        lv_label_set_text(ui_Label2, (char *)(String(day3.temmin) + "~" + String(day3.temmax) + "℃").c_str()); // 预告
    }
    else if (cs == 1)
    {
        // lv_obj_clear_flag(ui_Label1, LV_OBJ_FLAG_HIDDEN); /// Flags
        // lv_obj_clear_flag(ui_Label2, LV_OBJ_FLAG_HIDDEN); /// Flags
        lv_label_set_text(ui_Label1, (char *)(String(month()) + "月" + String(day()) + "日").c_str());
        lv_label_set_text(ui_Label2, (char *)(String(year()) + "年").c_str());
    }
    else if (cs == 2)
    {
        // lv_obj_clear_flag(ui_Label1, LV_OBJ_FLAG_HIDDEN); /// Flags
        // lv_obj_clear_flag(ui_Label2, LV_OBJ_FLAG_HIDDEN); /// Flags
        lv_label_set_text(ui_Label1, (char *)("星期" + String(weekStr[weekday() - 1])).c_str());
        lv_label_set_text(ui_Label2, "");
        lv_obj_set_align(ui_Label2, LV_ALIGN_RIGHT_MID);
        _ui_flag_modify(ui_Image99, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_REMOVE); // 现在天气
        weathericon(icon);
    }
    else if (cs == 3)
    {
        // lv_obj_clear_flag(ui_Label1, LV_OBJ_FLAG_HIDDEN); /// Flags
        // lv_obj_clear_flag(ui_Label2, LV_OBJ_FLAG_HIDDEN); /// Flags
        lv_label_set_text(ui_Label1, (char *)(String(today.weather)).c_str());                            // 现在天气
        lv_label_set_text(ui_Label2, (char *)("温" + String(wetemp) + " 湿" + String(humidity)).c_str()); // 现在温度和湿度
    }
    else if (cs == 4) // 显示温度湿度
    {
        // lv_obj_clear_flag(ui_Label1, LV_OBJ_FLAG_HIDDEN); /// Flags
        // lv_obj_clear_flag(ui_Label2, LV_OBJ_FLAG_HIDDEN); /// Flags
        lv_label_set_text(ui_Label1, (char *)("温度:" + String(Temperature)).c_str());
        lv_label_set_text(ui_Label2, (char *)("湿度:" + String(Humidityure)).c_str());
    }
    else if (cs == 5)
    {
        // lv_obj_clear_flag(ui_Label1, LV_OBJ_FLAG_HIDDEN); /// Flags
        // lv_obj_clear_flag(ui_Label2, LV_OBJ_FLAG_HIDDEN); /// Flags
        if (WiFi.status() == WL_CONNECTED)
        {
            lv_label_set_text(ui_Label1, (char *)(String(WiFi.RSSI())).c_str());
        }
        else
        {
            lv_label_set_text(ui_Label1, "NoWiFI");
        }
        // lv_label_set_text(ui_Label2, (char *)(String(num_update[0])+""+String(num_update[1])+""+String(num_update[2])+""+String(num_update[3])+""+String(num_update[4])+""+String(num_update[5])).c_str());
        lv_label_set_text(ui_Label2, (char *)(String(batVoltage) + "V").c_str());
    }

    Serial.println(cs);
    _ui_flag_modify(ui_Image0, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_ADD);
    _ui_flag_modify(ui_Image1, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_ADD);
    _ui_flag_modify(ui_Image2, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_ADD);
    _ui_flag_modify(ui_Image3, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_ADD);
    _ui_flag_modify(ui_Image4, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_ADD);
    _ui_flag_modify(ui_Image5, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_ADD);
    _ui_flag_modify(ui_Image6, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_ADD);
    _ui_flag_modify(ui_Image7, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_ADD);
    _ui_flag_modify(ui_Image8, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_ADD);
    _ui_flag_modify(ui_Image9, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_ADD);

    Serial.println(num_1);
    switch (num_1)
    {
    case 0:
        _ui_flag_modify(ui_Image0, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_REMOVE);
        break;
    case 1:
        _ui_flag_modify(ui_Image1, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_REMOVE);
        break;
    case 2:
        _ui_flag_modify(ui_Image2, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_REMOVE);
        break;
    case 3:
        _ui_flag_modify(ui_Image3, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_REMOVE);
        break;
    case 4:
        _ui_flag_modify(ui_Image4, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_REMOVE);
        break;
    case 5:
        _ui_flag_modify(ui_Image5, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_REMOVE);
        break;
    case 6:
        _ui_flag_modify(ui_Image6, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_REMOVE);
        break;
    case 7:
        _ui_flag_modify(ui_Image7, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_REMOVE);
        break;
    case 8:
        _ui_flag_modify(ui_Image8, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_REMOVE);
        break;
    case 9:
        _ui_flag_modify(ui_Image9, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_REMOVE);
        break;
    default:
        _ui_flag_modify(ui_Image0, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_REMOVE);
    }
}

void updateUi(int allupdate)
{
    if (allupdate == 1)
    {
        for (int j = 0; j < 6; j++)
        {
            num_update[j] = 1;
        }
    }
    else
    {
        // 依次更新画面
        if (num_update[0])
        { // 时1
            setScreen(0, num_0);
        }
        else if (num_update[1])
        { // 时2
            setScreen(1, num_1);
        }
        else if (num_update[2])
        { // 分1
            setScreen(2, num_2);
        }
        else if (num_update[3])
        { // 分2
            setScreen(3, num_3);
        }
        else if (num_update[4])
        { // 秒1
            setScreen(4, num_4);
        }
        else if (num_update[5])
        { // 秒2
            setScreen(5, num_5);
        }
    }

    int tmpnum = 0;
    // 更新数据
    tmpnum = hour() / 10;
    if (num_0 != tmpnum)
    { // 时1
        num_update[0] = 1;
        num_0 = tmpnum;
    }
    tmpnum = hour() % 10;
    if (num_1 != tmpnum)
    { // 时2
        num_update[1] = 1;
        num_1 = tmpnum;
    }
    tmpnum = minute() / 10;
    if (num_2 != tmpnum)
    { // 分1
        num_update[2] = 1;
        num_2 = tmpnum;
    }
    tmpnum = minute() % 10;
    if (num_3 != tmpnum)
    { // 分2
        num_update[3] = 1;
        num_3 = tmpnum;
    }
    tmpnum = second() / 10;
    if (num_4 != tmpnum)
    { // 秒1
        num_update[4] = 1;
        num_4 = tmpnum;
    }
    tmpnum = second() % 10;
    if (num_5 != tmpnum)
    { // 秒2
        num_update[5] = 1;
        num_5 = tmpnum;
    }
    taskYIELD();
}
