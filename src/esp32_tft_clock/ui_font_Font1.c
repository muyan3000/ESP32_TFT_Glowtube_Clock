/*******************************************************************************
 * Size: 14 px
 * Bpp: 1
 * Opts: --bpp 1 --size 14 --font K:\Qsync\LVGL\128x64B\assets\FONT\Deng.ttf -o K:\Qsync\LVGL\128x64B\assets\FONT\ui_font_Font1.c --format lvgl -r 0x20-0x7f --symbols 温湿度年月日星期一二三四五六天气晴多云暴大中小雨阴台风℃ --no-compress --no-prefilter
 ******************************************************************************/

#include "ui.h"

#ifndef UI_FONT_FONT1
#define UI_FONT_FONT1 1
#endif

#if UI_FONT_FONT1

/*-----------------
 *    BITMAPS
 *----------------*/

/*Store the image of the glyphs*/
static LV_ATTRIBUTE_LARGE_CONST const uint8_t glyph_bitmap[] = {
    /* U+0020 " " */
    0x0,

    /* U+0021 "!" */
    0xfe, 0x20,

    /* U+0022 "\"" */
    0xb4,

    /* U+0023 "#" */
    0x12, 0x22, 0x22, 0x7f, 0x24, 0x24, 0x24, 0xff,
    0x44, 0x48, 0x48,

    /* U+0024 "$" */
    0x75, 0x6b, 0x4e, 0x18, 0xa5, 0xad, 0x5c, 0x40,

    /* U+0025 "%" */
    0x45, 0x52, 0xa5, 0x8b, 0x57, 0x5a, 0x95, 0x2a,
    0x95, 0x10,

    /* U+0026 "&" */
    0x18, 0x12, 0x9, 0x4, 0x81, 0x83, 0x8b, 0x25,
    0x1a, 0x86, 0x63, 0x1e, 0x60,

    /* U+0027 "'" */
    0xc0,

    /* U+0028 "(" */
    0x29, 0x49, 0x24, 0x91, 0x22,

    /* U+0029 ")" */
    0x89, 0x12, 0x49, 0x25, 0x28,

    /* U+002A "*" */
    0x21, 0x3e, 0xa1, 0x0,

    /* U+002B "+" */
    0x10, 0x20, 0x47, 0xf1, 0x2, 0x4, 0x0,

    /* U+002C "," */
    0xe0,

    /* U+002D "-" */
    0xe0,

    /* U+002E "." */
    0x80,

    /* U+002F "/" */
    0x8, 0x44, 0x22, 0x10, 0x88, 0x42, 0x20,

    /* U+0030 "0" */
    0x79, 0x28, 0x61, 0x86, 0x18, 0x61, 0x85, 0x27,
    0x0,

    /* U+0031 "1" */
    0x65, 0x8, 0x42, 0x10, 0x84, 0x21, 0x3e,

    /* U+0032 "2" */
    0x7b, 0x38, 0x41, 0x4, 0x21, 0x8, 0x42, 0xf,
    0xc0,

    /* U+0033 "3" */
    0x79, 0x38, 0x41, 0xc, 0xc0, 0xc1, 0x87, 0x37,
    0x80,

    /* U+0034 "4" */
    0x4, 0x18, 0x50, 0xa2, 0x44, 0x91, 0x22, 0xfe,
    0x8, 0x10,

    /* U+0035 "5" */
    0xf4, 0x21, 0xf, 0x4c, 0x21, 0x8c, 0xdc,

    /* U+0036 "6" */
    0x39, 0x38, 0x20, 0xbb, 0x38, 0x61, 0x85, 0x37,
    0x80,

    /* U+0037 "7" */
    0xfc, 0x10, 0x82, 0x10, 0x41, 0x8, 0x20, 0x82,
    0x0,

    /* U+0038 "8" */
    0x7b, 0x38, 0x61, 0xcc, 0xcc, 0xe1, 0x87, 0x37,
    0x80,

    /* U+0039 "9" */
    0x73, 0x28, 0x61, 0x87, 0x37, 0x41, 0x7, 0x27,
    0x0,

    /* U+003A ":" */
    0x81,

    /* U+003B ";" */
    0x81, 0xc0,

    /* U+003C "<" */
    0x6, 0x31, 0x84, 0x4, 0x6, 0x3, 0x0,

    /* U+003D "=" */
    0xfe, 0x0, 0x0, 0xf, 0xe0,

    /* U+003E ">" */
    0x80, 0xc0, 0x60, 0x30, 0xc6, 0x30, 0x0,

    /* U+003F "?" */
    0x7b, 0x38, 0x41, 0x4, 0x21, 0x8, 0x0, 0x2,
    0x0,

    /* U+0040 "@" */
    0xf, 0x81, 0x4, 0x20, 0x24, 0x69, 0x49, 0x99,
    0x19, 0x91, 0x19, 0x11, 0x91, 0x39, 0x32, 0xcd,
    0xc4, 0x0, 0x20, 0xc1, 0xf0,

    /* U+0041 "A" */
    0x8, 0xa, 0x5, 0x2, 0x82, 0x21, 0x10, 0x88,
    0xfe, 0x41, 0x20, 0xa0, 0x20,

    /* U+0042 "B" */
    0xfa, 0x38, 0x61, 0x8b, 0xc8, 0xe1, 0x86, 0x3f,
    0x80,

    /* U+0043 "C" */
    0x3c, 0x62, 0xc0, 0x80, 0x80, 0x80, 0x80, 0x80,
    0x43, 0x62, 0x3c,

    /* U+0044 "D" */
    0xf8, 0x86, 0x82, 0x81, 0x81, 0x81, 0x81, 0x81,
    0x82, 0x86, 0xf8,

    /* U+0045 "E" */
    0xfc, 0x21, 0x8, 0x7e, 0x10, 0x84, 0x3e,

    /* U+0046 "F" */
    0xfc, 0x21, 0x8, 0x7e, 0x10, 0x84, 0x20,

    /* U+0047 "G" */
    0x3c, 0x62, 0xc1, 0x80, 0x80, 0x80, 0x8f, 0x81,
    0x41, 0x63, 0x3e,

    /* U+0048 "H" */
    0x83, 0x6, 0xc, 0x18, 0x3f, 0xe0, 0xc1, 0x83,
    0x6, 0x8,

    /* U+0049 "I" */
    0xff, 0xe0,

    /* U+004A "J" */
    0x31, 0x11, 0x11, 0x11, 0x11, 0xe0,

    /* U+004B "K" */
    0x85, 0x12, 0x44, 0x8a, 0x1c, 0x24, 0x48, 0x89,
    0x1a, 0x10,

    /* U+004C "L" */
    0x84, 0x21, 0x8, 0x42, 0x10, 0x84, 0x3e,

    /* U+004D "M" */
    0xc0, 0xf0, 0x3c, 0xe, 0x85, 0xa1, 0x6c, 0x99,
    0x26, 0x49, 0x8c, 0x63, 0x18, 0xc4,

    /* U+004E "N" */
    0xc1, 0xc1, 0xa1, 0xa1, 0x91, 0x89, 0x89, 0x85,
    0x85, 0x83, 0x83,

    /* U+004F "O" */
    0x3e, 0x31, 0x90, 0x50, 0x18, 0xc, 0x6, 0x3,
    0x1, 0x41, 0x31, 0x8f, 0x80,

    /* U+0050 "P" */
    0xfa, 0x38, 0x61, 0x86, 0x2f, 0xa0, 0x82, 0x8,
    0x0,

    /* U+0051 "Q" */
    0x3e, 0x31, 0x90, 0x50, 0x18, 0xc, 0x6, 0x3,
    0x1, 0x41, 0x31, 0x8f, 0x81, 0x80, 0x70,

    /* U+0052 "R" */
    0xfa, 0x38, 0x61, 0x86, 0x2f, 0x24, 0x8a, 0x28,
    0x40,

    /* U+0053 "S" */
    0x7a, 0x28, 0x20, 0x40, 0xc0, 0xc1, 0x86, 0x37,
    0x80,

    /* U+0054 "T" */
    0xfe, 0x20, 0x40, 0x81, 0x2, 0x4, 0x8, 0x10,
    0x20, 0x40,

    /* U+0055 "U" */
    0x83, 0x6, 0xc, 0x18, 0x30, 0x60, 0xc1, 0x82,
    0x89, 0xe0,

    /* U+0056 "V" */
    0x81, 0x41, 0x41, 0x42, 0x22, 0x22, 0x24, 0x14,
    0x14, 0x18, 0x8,

    /* U+0057 "W" */
    0x82, 0x12, 0x10, 0x91, 0x84, 0x8a, 0x24, 0x52,
    0x24, 0x90, 0xa4, 0x85, 0x14, 0x28, 0xc1, 0x86,
    0x4, 0x10,

    /* U+0058 "X" */
    0x42, 0x42, 0x24, 0x24, 0x18, 0x18, 0x18, 0x24,
    0x22, 0x42, 0x81,

    /* U+0059 "Y" */
    0x83, 0xd, 0x12, 0x42, 0x82, 0x4, 0x8, 0x10,
    0x20, 0x40,

    /* U+005A "Z" */
    0xfe, 0x4, 0x10, 0x40, 0x82, 0x8, 0x10, 0x41,
    0x3, 0xf8,

    /* U+005B "[" */
    0xf2, 0x49, 0x24, 0x92, 0x4e,

    /* U+005C "\\" */
    0x86, 0x10, 0x82, 0x10, 0x82, 0x10, 0x42,

    /* U+005D "]" */
    0xe4, 0x92, 0x49, 0x24, 0x9e,

    /* U+005E "^" */
    0x10, 0x50, 0xa1, 0x24, 0x48, 0xa0, 0x80,

    /* U+005F "_" */
    0xfc,

    /* U+0060 "`" */
    0x88,

    /* U+0061 "a" */
    0x76, 0x42, 0xf8, 0xc6, 0x6d,

    /* U+0062 "b" */
    0x82, 0x8, 0x2e, 0xce, 0x18, 0x61, 0x87, 0x3b,
    0x80,

    /* U+0063 "c" */
    0x7b, 0x28, 0x20, 0x82, 0xc, 0x9e,

    /* U+0064 "d" */
    0x4, 0x10, 0x5d, 0xce, 0x18, 0x61, 0x87, 0x37,
    0x40,

    /* U+0065 "e" */
    0x79, 0x38, 0x7f, 0x82, 0xc, 0x5e,

    /* U+0066 "f" */
    0x34, 0x4f, 0x44, 0x44, 0x44, 0x40,

    /* U+0067 "g" */
    0x77, 0x38, 0x61, 0x86, 0x1c, 0xdd, 0x85, 0xe0,

    /* U+0068 "h" */
    0x82, 0x8, 0x2e, 0xce, 0x18, 0x61, 0x86, 0x18,
    0x40,

    /* U+0069 "i" */
    0x9f, 0xe0,

    /* U+006A "j" */
    0x41, 0x55, 0x55, 0xc0,

    /* U+006B "k" */
    0x82, 0x8, 0x22, 0x92, 0x8e, 0x28, 0x92, 0x28,
    0x80,

    /* U+006C "l" */
    0xff, 0xe0,

    /* U+006D "m" */
    0xb3, 0x66, 0x62, 0x31, 0x18, 0x8c, 0x46, 0x23,
    0x11,

    /* U+006E "n" */
    0xbb, 0x38, 0x61, 0x86, 0x18, 0x61,

    /* U+006F "o" */
    0x38, 0x8a, 0xc, 0x18, 0x30, 0x51, 0x1c,

    /* U+0070 "p" */
    0xbb, 0x38, 0x61, 0x86, 0x1c, 0xae, 0x82, 0x0,

    /* U+0071 "q" */
    0x77, 0x38, 0x61, 0x86, 0x1c, 0xdd, 0x4, 0x10,

    /* U+0072 "r" */
    0xba, 0x49, 0x24,

    /* U+0073 "s" */
    0x74, 0x60, 0x83, 0x86, 0x2e,

    /* U+0074 "t" */
    0x44, 0xf4, 0x44, 0x44, 0x47,

    /* U+0075 "u" */
    0x86, 0x18, 0x61, 0x86, 0x1c, 0xdd,

    /* U+0076 "v" */
    0x86, 0x14, 0x52, 0x48, 0xa3, 0x4,

    /* U+0077 "w" */
    0x88, 0xa3, 0x24, 0xc9, 0x52, 0x55, 0x14, 0xc2,
    0x30, 0x84,

    /* U+0078 "x" */
    0x85, 0x23, 0xc, 0x30, 0xa4, 0xa1,

    /* U+0079 "y" */
    0x85, 0x14, 0x52, 0x28, 0xc1, 0x4, 0x23, 0x0,

    /* U+007A "z" */
    0xfc, 0x21, 0x4, 0x21, 0x8, 0x3f,

    /* U+007B "{" */
    0x74, 0x44, 0x44, 0x84, 0x44, 0x44, 0x70,

    /* U+007C "|" */
    0xff, 0xf8,

    /* U+007D "}" */
    0xe2, 0x22, 0x22, 0x12, 0x22, 0x22, 0xe0,

    /* U+007E "~" */
    0xe2, 0x38,

    /* U+2103 "℃" */
    0x63, 0xe9, 0x63, 0x94, 0x6, 0x40, 0x8, 0x0,
    0x80, 0x4, 0x0, 0x41, 0x6, 0x20, 0x3c,

    /* U+4E00 "一" */
    0xff, 0xf0,

    /* U+4E09 "三" */
    0x7f, 0xe0, 0x0, 0x0, 0x0, 0x0, 0x3f, 0xc0,
    0x0, 0x0, 0x0, 0x0, 0x0, 0xf, 0xff,

    /* U+4E2D "中" */
    0x4, 0x0, 0x80, 0x10, 0x7f, 0xf8, 0x43, 0x8,
    0x61, 0xf, 0xff, 0x84, 0x20, 0x80, 0x10, 0x2,
    0x0, 0x40,

    /* U+4E8C "二" */
    0x7f, 0xe0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,
    0x0, 0x0, 0x0, 0x0, 0xff, 0xf0,

    /* U+4E91 "云" */
    0x3f, 0xc0, 0x0, 0x0, 0x0, 0x0, 0xff, 0xf0,
    0x60, 0x4, 0x0, 0x80, 0x10, 0x82, 0x4, 0x3f,
    0xa0, 0x0,

    /* U+4E94 "五" */
    0x7f, 0xf0, 0x40, 0x4, 0x0, 0x40, 0x4, 0x3,
    0xfc, 0x8, 0x40, 0x84, 0x8, 0x40, 0x84, 0xff,
    0xf0,

    /* U+516D "六" */
    0x0, 0x0, 0x40, 0x2, 0x0, 0x20, 0xff, 0xf0,
    0x0, 0x0, 0x0, 0x90, 0x10, 0x83, 0x4, 0x20,
    0x64, 0x2, 0x80, 0x0,

    /* U+53F0 "台" */
    0x4, 0x1, 0x0, 0x44, 0x10, 0x44, 0x4, 0xff,
    0xc0, 0x1, 0xfe, 0x20, 0x44, 0x8, 0x81, 0x1f,
    0xe2, 0x4,

    /* U+56DB "四" */
    0xff, 0xe4, 0x99, 0x26, 0x49, 0x92, 0x68, 0xf8,
    0x6, 0x1, 0xff, 0xe0, 0x10,

    /* U+591A "多" */
    0x0, 0x0, 0x40, 0xf, 0xe1, 0x4, 0x64, 0x80,
    0x30, 0xe, 0xf, 0xa0, 0x47, 0xe0, 0x82, 0x34,
    0x40, 0x38, 0x7, 0x7, 0x80,

    /* U+5927 "大" */
    0x2, 0x0, 0x10, 0x0, 0x80, 0x4, 0x7, 0xff,
    0x1, 0x0, 0xc, 0x0, 0xa0, 0x4, 0x80, 0x46,
    0x4, 0x18, 0x40, 0x64, 0x0, 0x80,

    /* U+5929 "天" */
    0x3f, 0xf0, 0x10, 0x0, 0x80, 0x4, 0x7, 0xff,
    0x1, 0x80, 0xc, 0x0, 0x90, 0x4, 0x80, 0x42,
    0xc, 0xc, 0x80, 0x20,

    /* U+5C0F "小" */
    0x2, 0x0, 0x10, 0x0, 0x80, 0x4, 0x2, 0x24,
    0x11, 0x10, 0x88, 0x88, 0x42, 0x42, 0x16, 0x10,
    0x80, 0x80, 0x4, 0x0, 0xe0, 0x0,

    /* U+5E74 "年" */
    0x8, 0x0, 0x40, 0x7, 0xfe, 0x42, 0xc, 0x10,
    0xf, 0xf8, 0x44, 0x2, 0x20, 0x11, 0x3, 0xff,
    0xc0, 0x40, 0x2, 0x0, 0x10, 0x0,

    /* U+5EA6 "度" */
    0x0, 0x0, 0x8, 0xf, 0xfe, 0x48, 0x82, 0x44,
    0x1f, 0xf8, 0x91, 0x4, 0x70, 0x3f, 0xe1, 0x22,
    0x8, 0xa0, 0x86, 0xc, 0xcf, 0x8, 0xc,

    /* U+65E5 "日" */
    0xff, 0xc0, 0x60, 0x30, 0x18, 0xf, 0xfe, 0x3,
    0x1, 0x80, 0xff, 0xe0, 0x30, 0x0,

    /* U+661F "星" */
    0x3f, 0xe1, 0x1, 0xf, 0xf8, 0x40, 0x43, 0xfe,
    0x11, 0x0, 0xff, 0xc8, 0x40, 0x3f, 0xe0, 0x10,
    0x0, 0x81, 0xff, 0xf0,

    /* U+6674 "晴" */
    0x0, 0x87, 0xbf, 0xa4, 0x21, 0x2f, 0xc9, 0x8,
    0x7f, 0xbe, 0x5f, 0x92, 0x84, 0x97, 0xe7, 0xa1,
    0x25, 0xf8, 0x8, 0x40, 0x46, 0x0,

    /* U+66B4 "暴" */
    0x1f, 0xf0, 0x7f, 0xc1, 0x1, 0x7, 0xfc, 0x4,
    0x40, 0xff, 0xc7, 0xff, 0xc2, 0x48, 0x7d, 0x5c,
    0xe, 0x13, 0xd7, 0x0, 0xc4,

    /* U+6708 "月" */
    0x1f, 0xe2, 0x4, 0x40, 0x8f, 0xf1, 0x2, 0x20,
    0x47, 0xf8, 0x81, 0x30, 0x24, 0x4, 0x80, 0xa0,
    0x70,

    /* U+671F "期" */
    0x24, 0x1, 0x27, 0xdf, 0xa2, 0x49, 0x13, 0xcf,
    0x92, 0x44, 0xf2, 0x24, 0x9f, 0xff, 0x88, 0x4,
    0x49, 0x62, 0x82, 0x14, 0x21, 0x80,

    /* U+6C14 "气" */
    0x10, 0x0, 0x80, 0xf, 0xfc, 0x40, 0x4, 0x0,
    0x6f, 0xf0, 0x0, 0xf, 0xf8, 0x0, 0x40, 0x2,
    0x0, 0x10, 0x0, 0x80, 0x2, 0x0, 0x1c, 0x0,
    0x0,

    /* U+6E29 "温" */
    0x67, 0xe0, 0xc1, 0x1, 0xf9, 0x90, 0x42, 0x7e,
    0x0, 0x0, 0xbf, 0xc5, 0x52, 0x4a, 0x92, 0x54,
    0xaf, 0xfe,

    /* U+6E7F "湿" */
    0x0, 0x4, 0xfe, 0x28, 0x20, 0xfe, 0xc8, 0x24,
    0xfe, 0x2, 0x81, 0x2b, 0x4a, 0xa4, 0xac, 0x46,
    0xc4, 0x28, 0xbf, 0xf0,

    /* U+9634 "阴" */
    0xf7, 0xf2, 0x86, 0x90, 0xe3, 0xfa, 0x43, 0x28,
    0x65, 0xfc, 0xa1, 0xa4, 0x31, 0x6, 0x20, 0xc8,
    0x60,

    /* U+96E8 "雨" */
    0xff, 0xf8, 0x10, 0x1f, 0xf8, 0x84, 0x45, 0x2a,
    0x25, 0x51, 0x4c, 0x89, 0x54, 0x4a, 0x22, 0x11,
    0x10, 0xb8,

    /* U+98CE "风" */
    0x3f, 0xe0, 0x80, 0x82, 0xa, 0xe, 0x48, 0x25,
    0x20, 0x88, 0x82, 0x32, 0x9, 0x28, 0x68, 0xe1,
    0x40, 0x8c, 0x1, 0xa0, 0x6
};


/*---------------------
 *  GLYPH DESCRIPTION
 *--------------------*/

static const lv_font_fmt_txt_glyph_dsc_t glyph_dsc[] = {
    {.bitmap_index = 0, .adv_w = 0, .box_w = 0, .box_h = 0, .ofs_x = 0, .ofs_y = 0} /* id = 0 reserved */,
    {.bitmap_index = 0, .adv_w = 61, .box_w = 1, .box_h = 1, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 1, .adv_w = 64, .box_w = 1, .box_h = 11, .ofs_x = 2, .ofs_y = -1},
    {.bitmap_index = 3, .adv_w = 81, .box_w = 3, .box_h = 2, .ofs_x = 1, .ofs_y = 8},
    {.bitmap_index = 4, .adv_w = 134, .box_w = 8, .box_h = 11, .ofs_x = 0, .ofs_y = -1},
    {.bitmap_index = 15, .adv_w = 118, .box_w = 5, .box_h = 12, .ofs_x = 1, .ofs_y = -2},
    {.bitmap_index = 23, .adv_w = 112, .box_w = 7, .box_h = 11, .ofs_x = 1, .ofs_y = -1},
    {.bitmap_index = 33, .adv_w = 168, .box_w = 9, .box_h = 11, .ofs_x = 1, .ofs_y = -1},
    {.bitmap_index = 46, .adv_w = 50, .box_w = 1, .box_h = 2, .ofs_x = 1, .ofs_y = 8},
    {.bitmap_index = 47, .adv_w = 65, .box_w = 3, .box_h = 13, .ofs_x = 1, .ofs_y = -3},
    {.bitmap_index = 52, .adv_w = 65, .box_w = 3, .box_h = 13, .ofs_x = 1, .ofs_y = -3},
    {.bitmap_index = 57, .adv_w = 92, .box_w = 5, .box_h = 5, .ofs_x = 1, .ofs_y = 2},
    {.bitmap_index = 61, .adv_w = 149, .box_w = 7, .box_h = 7, .ofs_x = 1, .ofs_y = 1},
    {.bitmap_index = 68, .adv_w = 49, .box_w = 1, .box_h = 3, .ofs_x = 1, .ofs_y = -3},
    {.bitmap_index = 69, .adv_w = 112, .box_w = 3, .box_h = 1, .ofs_x = 2, .ofs_y = 3},
    {.bitmap_index = 70, .adv_w = 49, .box_w = 1, .box_h = 1, .ofs_x = 1, .ofs_y = -1},
    {.bitmap_index = 71, .adv_w = 85, .box_w = 5, .box_h = 11, .ofs_x = 0, .ofs_y = -1},
    {.bitmap_index = 78, .adv_w = 118, .box_w = 6, .box_h = 11, .ofs_x = 1, .ofs_y = -1},
    {.bitmap_index = 87, .adv_w = 118, .box_w = 5, .box_h = 11, .ofs_x = 1, .ofs_y = -1},
    {.bitmap_index = 94, .adv_w = 118, .box_w = 6, .box_h = 11, .ofs_x = 1, .ofs_y = -1},
    {.bitmap_index = 103, .adv_w = 118, .box_w = 6, .box_h = 11, .ofs_x = 0, .ofs_y = -1},
    {.bitmap_index = 112, .adv_w = 118, .box_w = 7, .box_h = 11, .ofs_x = 0, .ofs_y = -1},
    {.bitmap_index = 122, .adv_w = 118, .box_w = 5, .box_h = 11, .ofs_x = 1, .ofs_y = -1},
    {.bitmap_index = 129, .adv_w = 118, .box_w = 6, .box_h = 11, .ofs_x = 1, .ofs_y = -1},
    {.bitmap_index = 138, .adv_w = 118, .box_w = 6, .box_h = 11, .ofs_x = 1, .ofs_y = -1},
    {.bitmap_index = 147, .adv_w = 118, .box_w = 6, .box_h = 11, .ofs_x = 1, .ofs_y = -1},
    {.bitmap_index = 156, .adv_w = 118, .box_w = 6, .box_h = 11, .ofs_x = 1, .ofs_y = -1},
    {.bitmap_index = 165, .adv_w = 49, .box_w = 1, .box_h = 8, .ofs_x = 1, .ofs_y = -1},
    {.bitmap_index = 166, .adv_w = 49, .box_w = 1, .box_h = 10, .ofs_x = 1, .ofs_y = -3},
    {.bitmap_index = 168, .adv_w = 149, .box_w = 7, .box_h = 7, .ofs_x = 1, .ofs_y = 1},
    {.bitmap_index = 175, .adv_w = 149, .box_w = 7, .box_h = 5, .ofs_x = 1, .ofs_y = 2},
    {.bitmap_index = 180, .adv_w = 149, .box_w = 7, .box_h = 7, .ofs_x = 1, .ofs_y = 1},
    {.bitmap_index = 187, .adv_w = 98, .box_w = 6, .box_h = 11, .ofs_x = 0, .ofs_y = -1},
    {.bitmap_index = 196, .adv_w = 213, .box_w = 12, .box_h = 14, .ofs_x = 1, .ofs_y = -4},
    {.bitmap_index = 217, .adv_w = 143, .box_w = 9, .box_h = 11, .ofs_x = 0, .ofs_y = -1},
    {.bitmap_index = 230, .adv_w = 125, .box_w = 6, .box_h = 11, .ofs_x = 1, .ofs_y = -1},
    {.bitmap_index = 239, .adv_w = 139, .box_w = 8, .box_h = 11, .ofs_x = 1, .ofs_y = -1},
    {.bitmap_index = 250, .adv_w = 154, .box_w = 8, .box_h = 11, .ofs_x = 1, .ofs_y = -1},
    {.bitmap_index = 261, .adv_w = 113, .box_w = 5, .box_h = 11, .ofs_x = 1, .ofs_y = -1},
    {.bitmap_index = 268, .adv_w = 107, .box_w = 5, .box_h = 11, .ofs_x = 1, .ofs_y = -1},
    {.bitmap_index = 275, .adv_w = 152, .box_w = 8, .box_h = 11, .ofs_x = 1, .ofs_y = -1},
    {.bitmap_index = 286, .adv_w = 155, .box_w = 7, .box_h = 11, .ofs_x = 1, .ofs_y = -1},
    {.bitmap_index = 296, .adv_w = 55, .box_w = 1, .box_h = 11, .ofs_x = 1, .ofs_y = -1},
    {.bitmap_index = 298, .adv_w = 76, .box_w = 4, .box_h = 11, .ofs_x = 0, .ofs_y = -1},
    {.bitmap_index = 304, .adv_w = 124, .box_w = 7, .box_h = 11, .ofs_x = 1, .ofs_y = -1},
    {.bitmap_index = 314, .adv_w = 104, .box_w = 5, .box_h = 11, .ofs_x = 1, .ofs_y = -1},
    {.bitmap_index = 321, .adv_w = 194, .box_w = 10, .box_h = 11, .ofs_x = 1, .ofs_y = -1},
    {.bitmap_index = 335, .adv_w = 163, .box_w = 8, .box_h = 11, .ofs_x = 1, .ofs_y = -1},
    {.bitmap_index = 346, .adv_w = 170, .box_w = 9, .box_h = 11, .ofs_x = 1, .ofs_y = -1},
    {.bitmap_index = 359, .adv_w = 124, .box_w = 6, .box_h = 11, .ofs_x = 1, .ofs_y = -1},
    {.bitmap_index = 368, .adv_w = 170, .box_w = 9, .box_h = 13, .ofs_x = 1, .ofs_y = -3},
    {.bitmap_index = 383, .adv_w = 129, .box_w = 6, .box_h = 11, .ofs_x = 1, .ofs_y = -1},
    {.bitmap_index = 392, .adv_w = 115, .box_w = 6, .box_h = 11, .ofs_x = 1, .ofs_y = -1},
    {.bitmap_index = 401, .adv_w = 116, .box_w = 7, .box_h = 11, .ofs_x = 0, .ofs_y = -1},
    {.bitmap_index = 411, .adv_w = 150, .box_w = 7, .box_h = 11, .ofs_x = 1, .ofs_y = -1},
    {.bitmap_index = 421, .adv_w = 137, .box_w = 8, .box_h = 11, .ofs_x = 0, .ofs_y = -1},
    {.bitmap_index = 432, .adv_w = 205, .box_w = 13, .box_h = 11, .ofs_x = 0, .ofs_y = -1},
    {.bitmap_index = 450, .adv_w = 129, .box_w = 8, .box_h = 11, .ofs_x = 0, .ofs_y = -1},
    {.bitmap_index = 461, .adv_w = 122, .box_w = 7, .box_h = 11, .ofs_x = 0, .ofs_y = -1},
    {.bitmap_index = 471, .adv_w = 128, .box_w = 7, .box_h = 11, .ofs_x = 1, .ofs_y = -1},
    {.bitmap_index = 481, .adv_w = 65, .box_w = 3, .box_h = 13, .ofs_x = 1, .ofs_y = -3},
    {.bitmap_index = 486, .adv_w = 83, .box_w = 5, .box_h = 11, .ofs_x = 0, .ofs_y = -1},
    {.bitmap_index = 493, .adv_w = 65, .box_w = 3, .box_h = 13, .ofs_x = 0, .ofs_y = -3},
    {.bitmap_index = 498, .adv_w = 149, .box_w = 7, .box_h = 7, .ofs_x = 1, .ofs_y = 3},
    {.bitmap_index = 505, .adv_w = 93, .box_w = 6, .box_h = 1, .ofs_x = 0, .ofs_y = -3},
    {.bitmap_index = 506, .adv_w = 58, .box_w = 3, .box_h = 2, .ofs_x = 1, .ofs_y = 8},
    {.bitmap_index = 507, .adv_w = 112, .box_w = 5, .box_h = 8, .ofs_x = 1, .ofs_y = -1},
    {.bitmap_index = 512, .adv_w = 129, .box_w = 6, .box_h = 11, .ofs_x = 1, .ofs_y = -1},
    {.bitmap_index = 521, .adv_w = 101, .box_w = 6, .box_h = 8, .ofs_x = 1, .ofs_y = -1},
    {.bitmap_index = 527, .adv_w = 129, .box_w = 6, .box_h = 11, .ofs_x = 1, .ofs_y = -1},
    {.bitmap_index = 536, .adv_w = 115, .box_w = 6, .box_h = 8, .ofs_x = 1, .ofs_y = -1},
    {.bitmap_index = 542, .adv_w = 66, .box_w = 4, .box_h = 11, .ofs_x = 1, .ofs_y = -1},
    {.bitmap_index = 548, .adv_w = 129, .box_w = 6, .box_h = 10, .ofs_x = 1, .ofs_y = -3},
    {.bitmap_index = 556, .adv_w = 123, .box_w = 6, .box_h = 11, .ofs_x = 1, .ofs_y = -1},
    {.bitmap_index = 565, .adv_w = 50, .box_w = 1, .box_h = 11, .ofs_x = 1, .ofs_y = -1},
    {.bitmap_index = 567, .adv_w = 50, .box_w = 2, .box_h = 13, .ofs_x = 0, .ofs_y = -3},
    {.bitmap_index = 571, .adv_w = 105, .box_w = 6, .box_h = 11, .ofs_x = 1, .ofs_y = -1},
    {.bitmap_index = 580, .adv_w = 50, .box_w = 1, .box_h = 11, .ofs_x = 1, .ofs_y = -1},
    {.bitmap_index = 582, .adv_w = 188, .box_w = 9, .box_h = 8, .ofs_x = 1, .ofs_y = -1},
    {.bitmap_index = 591, .adv_w = 123, .box_w = 6, .box_h = 8, .ofs_x = 1, .ofs_y = -1},
    {.bitmap_index = 597, .adv_w = 128, .box_w = 7, .box_h = 8, .ofs_x = 1, .ofs_y = -1},
    {.bitmap_index = 604, .adv_w = 129, .box_w = 6, .box_h = 10, .ofs_x = 1, .ofs_y = -3},
    {.bitmap_index = 612, .adv_w = 129, .box_w = 6, .box_h = 10, .ofs_x = 1, .ofs_y = -3},
    {.bitmap_index = 620, .adv_w = 76, .box_w = 3, .box_h = 8, .ofs_x = 1, .ofs_y = -1},
    {.bitmap_index = 623, .adv_w = 91, .box_w = 5, .box_h = 8, .ofs_x = 1, .ofs_y = -1},
    {.bitmap_index = 628, .adv_w = 71, .box_w = 4, .box_h = 10, .ofs_x = 0, .ofs_y = -1},
    {.bitmap_index = 633, .adv_w = 123, .box_w = 6, .box_h = 8, .ofs_x = 1, .ofs_y = -1},
    {.bitmap_index = 639, .adv_w = 104, .box_w = 6, .box_h = 8, .ofs_x = 0, .ofs_y = -1},
    {.bitmap_index = 645, .adv_w = 157, .box_w = 10, .box_h = 8, .ofs_x = 0, .ofs_y = -1},
    {.bitmap_index = 655, .adv_w = 99, .box_w = 6, .box_h = 8, .ofs_x = 0, .ofs_y = -1},
    {.bitmap_index = 661, .adv_w = 105, .box_w = 6, .box_h = 10, .ofs_x = 0, .ofs_y = -3},
    {.bitmap_index = 669, .adv_w = 103, .box_w = 6, .box_h = 8, .ofs_x = 1, .ofs_y = -1},
    {.bitmap_index = 675, .adv_w = 65, .box_w = 4, .box_h = 13, .ofs_x = 0, .ofs_y = -3},
    {.bitmap_index = 682, .adv_w = 52, .box_w = 1, .box_h = 13, .ofs_x = 1, .ofs_y = -3},
    {.bitmap_index = 684, .adv_w = 65, .box_w = 4, .box_h = 13, .ofs_x = 0, .ofs_y = -3},
    {.bitmap_index = 691, .adv_w = 149, .box_w = 7, .box_h = 2, .ofs_x = 1, .ofs_y = 3},
    {.bitmap_index = 693, .adv_w = 224, .box_w = 12, .box_h = 10, .ofs_x = 1, .ofs_y = -1},
    {.bitmap_index = 708, .adv_w = 224, .box_w = 12, .box_h = 1, .ofs_x = 1, .ofs_y = 4},
    {.bitmap_index = 710, .adv_w = 224, .box_w = 12, .box_h = 10, .ofs_x = 1, .ofs_y = -1},
    {.bitmap_index = 725, .adv_w = 224, .box_w = 11, .box_h = 13, .ofs_x = 2, .ofs_y = -2},
    {.bitmap_index = 743, .adv_w = 224, .box_w = 12, .box_h = 9, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 757, .adv_w = 224, .box_w = 12, .box_h = 12, .ofs_x = 1, .ofs_y = -2},
    {.bitmap_index = 775, .adv_w = 224, .box_w = 12, .box_h = 11, .ofs_x = 1, .ofs_y = -1},
    {.bitmap_index = 792, .adv_w = 224, .box_w = 12, .box_h = 13, .ofs_x = 1, .ofs_y = -2},
    {.bitmap_index = 812, .adv_w = 224, .box_w = 11, .box_h = 13, .ofs_x = 1, .ofs_y = -2},
    {.bitmap_index = 830, .adv_w = 224, .box_w = 10, .box_h = 10, .ofs_x = 2, .ofs_y = -1},
    {.bitmap_index = 843, .adv_w = 224, .box_w = 12, .box_h = 14, .ofs_x = 1, .ofs_y = -2},
    {.bitmap_index = 864, .adv_w = 224, .box_w = 13, .box_h = 13, .ofs_x = 0, .ofs_y = -2},
    {.bitmap_index = 886, .adv_w = 224, .box_w = 13, .box_h = 12, .ofs_x = 0, .ofs_y = -2},
    {.bitmap_index = 906, .adv_w = 224, .box_w = 13, .box_h = 13, .ofs_x = 1, .ofs_y = -2},
    {.bitmap_index = 928, .adv_w = 224, .box_w = 13, .box_h = 13, .ofs_x = 0, .ofs_y = -2},
    {.bitmap_index = 950, .adv_w = 224, .box_w = 13, .box_h = 14, .ofs_x = 0, .ofs_y = -2},
    {.bitmap_index = 973, .adv_w = 224, .box_w = 9, .box_h = 12, .ofs_x = 2, .ofs_y = -2},
    {.bitmap_index = 987, .adv_w = 224, .box_w = 13, .box_h = 12, .ofs_x = 1, .ofs_y = -2},
    {.bitmap_index = 1007, .adv_w = 224, .box_w = 13, .box_h = 13, .ofs_x = 1, .ofs_y = -2},
    {.bitmap_index = 1029, .adv_w = 224, .box_w = 14, .box_h = 12, .ofs_x = 0, .ofs_y = -2},
    {.bitmap_index = 1050, .adv_w = 224, .box_w = 11, .box_h = 12, .ofs_x = 0, .ofs_y = -2},
    {.bitmap_index = 1067, .adv_w = 224, .box_w = 13, .box_h = 13, .ofs_x = 0, .ofs_y = -2},
    {.bitmap_index = 1089, .adv_w = 224, .box_w = 13, .box_h = 15, .ofs_x = 1, .ofs_y = -3},
    {.bitmap_index = 1114, .adv_w = 224, .box_w = 13, .box_h = 11, .ofs_x = 0, .ofs_y = -1},
    {.bitmap_index = 1132, .adv_w = 224, .box_w = 12, .box_h = 13, .ofs_x = 1, .ofs_y = -2},
    {.bitmap_index = 1152, .adv_w = 224, .box_w = 11, .box_h = 12, .ofs_x = 2, .ofs_y = -2},
    {.bitmap_index = 1169, .adv_w = 224, .box_w = 13, .box_h = 11, .ofs_x = 1, .ofs_y = -1},
    {.bitmap_index = 1187, .adv_w = 224, .box_w = 14, .box_h = 12, .ofs_x = 1, .ofs_y = -2}
};

/*---------------------
 *  CHARACTER MAPPING
 *--------------------*/

static const uint16_t unicode_list_1[] = {
    0x0, 0x2cfd, 0x2d06, 0x2d2a, 0x2d89, 0x2d8e, 0x2d91, 0x306a,
    0x32ed, 0x35d8, 0x3817, 0x3824, 0x3826, 0x3b0c, 0x3d71, 0x3da3,
    0x44e2, 0x451c, 0x4571, 0x45b1, 0x4605, 0x461c, 0x4b11, 0x4d26,
    0x4d7c, 0x7531, 0x75e5, 0x77cb
};

/*Collect the unicode lists and glyph_id offsets*/
static const lv_font_fmt_txt_cmap_t cmaps[] =
{
    {
        .range_start = 32, .range_length = 95, .glyph_id_start = 1,
        .unicode_list = NULL, .glyph_id_ofs_list = NULL, .list_length = 0, .type = LV_FONT_FMT_TXT_CMAP_FORMAT0_TINY
    },
    {
        .range_start = 8451, .range_length = 30668, .glyph_id_start = 96,
        .unicode_list = unicode_list_1, .glyph_id_ofs_list = NULL, .list_length = 28, .type = LV_FONT_FMT_TXT_CMAP_SPARSE_TINY
    }
};



/*--------------------
 *  ALL CUSTOM DATA
 *--------------------*/

#if LV_VERSION_CHECK(8, 0, 0)
/*Store all the custom data of the font*/
static  lv_font_fmt_txt_glyph_cache_t cache;
static const lv_font_fmt_txt_dsc_t font_dsc = {
#else
static lv_font_fmt_txt_dsc_t font_dsc = {
#endif
    .glyph_bitmap = glyph_bitmap,
    .glyph_dsc = glyph_dsc,
    .cmaps = cmaps,
    .kern_dsc = NULL,
    .kern_scale = 0,
    .cmap_num = 2,
    .bpp = 1,
    .kern_classes = 0,
    .bitmap_format = 0,
#if LV_VERSION_CHECK(8, 0, 0)
    .cache = &cache
#endif
};


/*-----------------
 *  PUBLIC FONT
 *----------------*/

/*Initialize a public general font descriptor*/
#if LV_VERSION_CHECK(8, 0, 0)
const lv_font_t ui_font_Font1 = {
#else
lv_font_t ui_font_Font1 = {
#endif
    .get_glyph_dsc = lv_font_get_glyph_dsc_fmt_txt,    /*Function pointer to get glyph's data*/
    .get_glyph_bitmap = lv_font_get_bitmap_fmt_txt,    /*Function pointer to get glyph's bitmap*/
    .line_height = 16,          /*The maximum line height required by the font*/
    .base_line = 4,             /*Baseline measured from the bottom of the line*/
#if !(LVGL_VERSION_MAJOR == 6 && LVGL_VERSION_MINOR == 0)
    .subpx = LV_FONT_SUBPX_NONE,
#endif
#if LV_VERSION_CHECK(7, 4, 0) || LVGL_VERSION_MAJOR >= 8
    .underline_position = -2,
    .underline_thickness = 1,
#endif
    .dsc = &font_dsc           /*The custom font data. Will be accessed by `get_glyph_bitmap/dsc` */
};



#endif /*#if UI_FONT_FONT1*/
