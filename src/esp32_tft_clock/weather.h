#ifndef _wheather_H_
#define _wheather_H_
#include "WiFiClientSecure.h"
#include <WiFiClient.h>
#include <ArduinoJson.h>
#include "ArduinoUZlib.h" // gzip库

#include "weather.h"

// #define Debug

#define uchar unsigned char
#define uint unsigned int
#define get_frequency_MAX 5

void read_yesteday_wheather(void);
uchar compare(const char *a, const char *b);
void warn(void);
bool Uv_get(void);             // 获取紫外线强度
bool wheather_get(void);       // 获取当前天气
bool Uforecast_get(void);      // 获取三天天气预报
void wheather_Uv_get(void);    // 获取天气和紫外线
void readcity_getCityid(void); // 读取城市名获取城市ID
void beginwheather_Uv_get(void);

String Https_get_Nzip(String url);
char *Https_get_zip(String url);
bool Https_get_zip(String url, uint8_t *&outbuf);

void parseNowJson(unsigned char d);
void parseNowJson(String payload, unsigned char d);
bool Cityid_get(String province, String county, String requserKey, String reqLang); // 获取城市ID号

const String UserKey = ""; // 私钥获取网址https://dev.heweather.com/docs/start/get-api-key

String city_ID_str="101020200"; // 城市id信息
String longitude;   // 经度
String latitude;    // 维度
uchar lola = 0;     // 使用经纬度标志位

typedef struct
{
  String weather = "NA";
  String weathernight = "NA";
  int temmax = 0;
  int temmin = 0;
  uchar Uv = 0;
} Forecast;

Forecast yesterday; // 昨天的数据
Forecast today;     // 今天的数据
Forecast day3;     // 3天预告的数据
uint8_t *payload_z = NULL;

void beginwheather_Uv_get(void)
{
  // Uv_get();
  // wheather_get();
  extern uchar flag;
  uchar kk = 0;
  flag = 0;

  // location_store(longitude, latitude, 0); //获取成功储存经纬度
  if (lola == 0)
  {
    char a[20], b[20];
    //location_read(&a[0], &b[0], 1);
    String County_j(a);
    String Provinc_j(b);
    taskYIELD();
    if (Cityid_get(County_j, Provinc_j, UserKey, "en") == true){
      //location_store(city_ID_str, "0", 2); // 储存城市ID
    }else
    {
      flag = 0x08;
      return;
    }
  }
  taskYIELD();
  if (Uv_get() == false)
    kk = 0x01;
  taskYIELD();
  if (wheather_get() == false)
    kk |= 0x02;
  taskYIELD();

  if ((kk & 0x01) == 0x01 && (kk & 0x02) == 0x02)
    flag = 0x08;
  else if ((kk & 0x01) == 0x01 && (kk & 0x02) == 0)
  {
    taskYIELD();
    if (Uv_get() == false)
      flag = 0x08;
    taskYIELD();
  }
  else if ((kk & 0x01) == 0 && (kk & 0x10) == 0x02)
  {
    taskYIELD();
    if (wheather_get() == false)
      flag = 0x08;
    taskYIELD();
  }
  // Serial.printf("flag=%d\n", flag);
  if (flag != 0x08){
    //location_store(longitude, latitude, 0); // 获取成功储存经纬度
  }
  taskYIELD();
}

void readcity_getCityid(void)
{
  char a[20], b[20];
  //location_read(&a[0], &b[0], 1);
  String County_j(a);
  String Provinc_j(b);
  taskYIELD();
  delay(800);
  if (Cityid_get(County_j, Provinc_j, UserKey, "en") == true){
    //location_store(city_ID_str, "0", 2); // 储存城市ID
  }
}

void wheather_Uv_get(void)
{
  taskYIELD();
  Uv_get();
  taskYIELD();
  wheather_get();
  taskYIELD();
}

// 尝试获取城市id信息，成功返回true，失败返回false
//  String province;     // 省
//  String county;       // 县
//  String requserKey;  // 私钥
//  String reqLang;     // 语言
bool Cityid_get(String province, String county, String requserKey, String reqLang)
{
  String payload_s;
  String url = "https://geoapi.qweather.com/v2/city/lookup?location=" + county + "&adm=" + province + "&lang=" + reqLang + "&key=" + requserKey;
  payload_s = Https_get_Nzip(url);
  if (payload_s == NULL)
    return false;
  parseNowJson(payload_s, 1); // json解析
#ifdef Debug
  Serial.println("url:\n");
  Serial.println(url);
  Serial.println("Data:\n");
  Serial.println(payload_s);
#endif
  return true;
}

// 尝试获取天气信息，成功返回true，失败返回false
bool wheather_get(void) // 获取天气
{
  String url;
  // uint8_t *payload = NULL;

  if (lola == 1)
    url = "https://devapi.qweather.com/v7/weather/now?location=" + longitude + "," + latitude + "&key=" + UserKey; //;
  else
    url = "https://devapi.qweather.com/v7/weather/now?location=" + city_ID_str + "&key=" + UserKey; //;

  if (Https_get_zip(url, payload_z) == false)
    return false;

#ifdef Debug
  Serial.println(url.c_str());
  Serial.printf("\n天气   %s\n", payload_z);
#endif
  parseNowJson(2); // json解析
  return true;
}

// 获取紫外线强度
bool Uv_get(void)
{
  String url;
  // uint8_t  *payload =NULL;
  if (lola == 1)
    url = "https://devapi.qweather.com/v7/indices/1d?type=5&location=" + longitude + "," + latitude + "&key=" + UserKey;
  else
    url = "https://devapi.qweather.com/v7/indices/1d?type=5&location=" + city_ID_str + "&key=" + UserKey;

  // payload = Https_get_zip(url);
  if (Https_get_zip(url, payload_z) == false)
  {
    return false;
  }
#ifdef Debug
  Serial.printf("紫外线: %s\n", payload_z);
#endif
  parseNowJson(3); // json解析
  return true;
}

// 获取三天天气预报
bool Uforecast_get(void)
{
  String url;
  // uint8_t *payload = NULL;
  if (lola == 1)
    url = "https://devapi.qweather.com/v7/weather/3d?location=" + longitude + "," + latitude + "&key=" + UserKey;
  else
    url = "https://devapi.qweather.com/v7/weather/3d?location=" + city_ID_str + "&key=" + UserKey;
  // payload = Https_get_zip(url);
  if (Https_get_zip(url, payload_z) == false)
    return false;
  parseNowJson(4); // json解析

#ifdef Debug
  Serial.println(ESP.getFreeHeap());
  Serial.println(url);
  Serial.printf("\n%s\n", payload_z);
#endif
  return true;
}
//{"code":"200","updateTime":"2023-06-25T17:27+08:00","fxLink":"https://www.qweather.com/weather/minhang-101020200.html","now":{"obsTime":"2023-06-25T17:10+08:00","temp":"29","feelsLike":"29","icon":"104","text":"阴","wind360":"212","windDir":"西南风","windScale":"3","windSpeed":"13","humidity":"61","precip":"0.0","pressure":"1005","vis":"30","cloud":"91","dew":"24"},"refer":{"sources":["QWeather","NMC","ECMWF"],"license":["CC BY-SA 4.0"]}}
//{"code":"200","updateTime":"2023-06-25T20:35+08:00","fxLink":"https://www.qweather.com/weather/minhang-101020200.html","daily":[{"fxDate":"2023-06-25","sunrise":"04:52","sunset":"19:03","moonrise":"10:56","moonset":"23:44","moonPhase":"峨眉月","moonPhaseIcon":"801","tempMax":"29","tempMin":"23","iconDay":"305","textDay":"小雨","iconNight":"305","textNight":"小雨","wind360Day":"202","windDirDay":"西南风","windScaleDay":"3-4","windSpeedDay":"19","wind360Night":"225","windDirNight":"西南风","windScaleNight":"1-2","windSpeedNight":"3","humidity":"84","precip":"1.0","pressure":"1006","vis":"24","cloud":"55","uvIndex":"3"},{"fxDate":"2023-06-26","sunrise":"04:53","sunset":"19:03","moonrise":"11:52","moonset":"","moonPhase":"上弦月","moonPhaseIcon":"802","tempMax":"31","tempMin":"23","iconDay":"305","textDay":"小雨","iconNight":"104","textNight":"阴","wind360Day":"225","windDirDay":"西南风","windScaleDay":"1-2","windSpeedDay":"3","wind360Night":"180","windDirNight":"南风","windScaleNight":"1-2","windSpeedNight":"3","humidity":"92","precip":"2.5","pressure":"1008","vis":"20","cloud":"64","uvIndex":"4"},{"fxDate":"2023-06-27","sunrise":"04:53","sunset":"19:03","moonrise":"12:48","moonset":"00:10","moonPhase":"盈凸月","moonPhaseIcon":"803","tempMax":"31","tempMin":"24","iconDay":"305","textDay":"小雨","iconNight":"150","textNight":"晴","wind360Day":"180","windDirDay":"南风","windScaleDay":"1-2","windSpeedDay":"3","wind360Night":"180","windDirNight":"南风","windScaleNight":"1-2","windSpeedNight":"3","humidity":"87","precip":"5.1","pressure":"1008","vis":"24","cloud":"80","uvIndex":"9"}],"refer":{"sources":["QWeather","NMC","ECMWF"],"license":["CC BY-SA 4.0"]}}
// 解析Json信息
void parseNowJson(unsigned char d)
{
  extern unsigned char humidity; // 湿度
  extern unsigned char Uv;       // 紫外线强度
  extern int wetemp;               // 温度
  extern String weather;
  //unsigned int icon;
  char *ssa = NULL;

  taskYIELD();

#ifdef Debug
  Serial.printf("\n JSON num %d\n", d);
#endif
  switch (d)
  {
  case 1:
  {
    char get_data[9];
    ssa = strstr((const char *)payload_z, "id") + 5;
    memcpy(get_data, (const char *)ssa, 9);
    city_ID_str = String(get_data);
    return;
  }
  case 2:
  {
    char get_weather[25];
    uint8_t fklp = 0;
    memset(get_weather, 0, 25);
    ssa = strstr((const char *)payload_z, "temp") + 7;
    wetemp = ((*(ssa)-0x30) * 10) + ((*(ssa + 1) - 0x30));
    ssa = strstr((const char *)payload_z, "icon") + 7;
    icon = ((*(ssa)-0x30) * 100) + ((*(ssa + 1) - 0x30) * 10) + ((*(ssa + 2) - 0x30));
    ssa = strstr((const char *)payload_z, "text") + 7;
    while (*ssa != '"' && fklp <= 24)
      get_weather[fklp++] = *ssa++;
    //city_ID_str = String(get_weather);
    today.weather = String(get_weather);
    ssa = strstr((const char *)payload_z, "humidity") + 11;
    humidity = ((*(ssa)-0x30) * 10) + ((*(ssa + 1) - 0x30));
    taskYIELD();
    //Oled_weathericon(icon);
  }
    return;
  case 3:
    ssa = strstr((const char *)payload_z, "level") + 8;
    Uv = (*ssa) - 0x30;
    return;
  case 4:
  {
    char get_weather[25];
    uint8_t fklp = 0;
    memset(get_weather, 0, 25);
    ssa = strstr((const char *)payload_z, "tempMax") + 10;
    day3.temmax = ((*(ssa)-0x30) * 10) + ((*(ssa + 1) - 0x30));

    ssa = strstr((const char *)ssa, "tempMin") + 10;
    day3.temmin = ((*(ssa)-0x30) * 10) + ((*(ssa + 1) - 0x30));

    ssa = strstr((const char *)ssa, "textDay") + 10;
    while (*ssa != '"' && fklp <= 24)
      get_weather[fklp++] = *ssa++;
    day3.weather = String(get_weather);

    memset(get_weather, 0, 25);
    fklp = 0;
    ssa = strstr((const char *)ssa, "textNight") + 12;
    while (*ssa != '"' && fklp <= 24)
      get_weather[fklp++] = *ssa++;
    day3.weathernight = String(get_weather);

    ssa = strstr((const char *)ssa, "uvIndex") + 10;
    day3.Uv = ((*(ssa)-0x30));
    taskYIELD();
  }

    return;
  }

  //  Serial.println(ESP.getFreeHeap());
}

// 解析Json信息
void parseNowJson(String payload, unsigned char d)
{
  extern unsigned char humidity; // 湿度
  extern unsigned char Uv;       // 紫外线强度
  extern int wetemp;               // 温度
  extern String weather;
  //unsigned int icon;
  char *ssa = NULL;

  // #define Json_analysis
  taskYIELD();
#ifdef Json_analysis
  const size_t capacity = 1000;
  DynamicJsonDocument doc(capacity);
  deserializeJson(doc, payload);
#endif
  switch (d)
  {
  case 1:
  {
#ifdef Json_analysis
    city_ID_str = doc["location"][0]["id"].as<String>(); // 城市id
                                                         //   city_ID_nam = doc["location"][0]["name"].as<String>(); // 城市名
#else
    char get_data[9];
    ssa = strstr((const char *)payload_z, "id") + 5;
    memcpy(get_data, (const char *)ssa, 9);
    city_ID_str = String(get_data);
#endif
    return;
  }
  case 2:
  {
#ifdef Json_analysis
    JsonObject now = doc["now"];
    weather = now["text"].as<String>();             // 文字描述
    icon = now["icon"].as<unsigned int>();          // 天气码
    wetemp = now["temp"].as<int>();                   // 温度
    humidity = now["humidity"].as<unsigned char>(); // 湿度
#else
    char get_weather[25];
    uint8_t fklp = 0;
    memset(get_weather, 0, 25);
    ssa = strstr((const char *)payload_z, "temp") + 7;
    wetemp = ((*(ssa)-0x30) * 10) + ((*(ssa + 1) - 0x30));
    ssa = strstr((const char *)payload_z, "icon") + 7;
    icon = ((*(ssa)-0x30) * 100) + ((*(ssa + 1) - 0x30) * 10) + ((*(ssa + 2) - 0x30));
    ssa = strstr((const char *)payload_z, "text") + 7;
    while (*ssa != '"' && fklp <= 24)
      get_weather[fklp++] = *ssa++;
    //city_ID_str = String(get_weather);
    today.weather = String(get_weather);
    ssa = strstr((const char *)payload_z, "humidity") + 11;
    humidity = ((*(ssa)-0x30) * 10) + ((*(ssa + 1) - 0x30));
#endif
    taskYIELD();
    //Oled_weathericon(icon);
  }

  case 3:

#ifdef Json_analysis
    Uv = doc["daily"][0]["level"].as<unsigned char>();
#else
    ssa = strstr((const char *)payload_z, "level") + 8;
    Uv = (*ssa) - 0x30;
#endif
    return;
  case 4:
#ifdef Json_analysis
    String we = doc["daily"][0]["textDay"].as<String>();
    today.temmax = doc["daily"][0]["tempMax"].as<int>();
    today.temmin = doc["daily"][0]["tempMin"].as<int>();
    today.Uv = doc["daily"][0]["uvIndex"].as<unsigned char>();
    today.weather = we;
#else
    char get_weather[25];
    uint8_t fklp = 0;
    memset(get_weather, 0, 25);
    ssa = strstr((const char *)payload_z, "tempMax") + 10;
    day3.temmax = ((*(ssa)-0x30) * 10) + ((*(ssa + 1) - 0x30));
    ssa = strstr((const char *)ssa, "tempMin") + 10;
    day3.temmin = ((*(ssa)-0x30) * 10) + ((*(ssa + 1) - 0x30));
    ssa = strstr((const char *)ssa, "textDay") + 10;
    while (*ssa != '"' && fklp <= 24)
      get_weather[fklp++] = *ssa++;
    day3.weather = String(get_weather);

    fklp = 0;
    memset(get_weather, 0, 25);
    ssa = strstr((const char *)ssa, "textNight") + 12;
    while (*ssa != '"' && fklp <= 24)
      get_weather[fklp++] = *ssa++;
    day3.weathernight = String(get_weather);

    ssa = strstr((const char *)ssa, "uvIndex") + 10;
    day3.Uv = ((*(ssa)-0x30));
#endif
    taskYIELD();
    return;
  }
}

// bool fetchBuffer(const char *url)
// {
//   uint16_t _bufferSize=0;
//   //_bufferSize = 0;
//   std::unique_ptr<WiFiClientSecure> client(new WiFiClientSecure);
//   client->setInsecure();
//   HTTPClient https;
//   if (https.begin(*client, url))
//   {
//     https.addHeader("Accept-Encoding", "gzip");
//     int httpCode = https.GET();
//     if (httpCode > 0)
//     {
//       if (httpCode == HTTP_CODE_OK)
//       {
//         int len = https.getSize();      // get length of document (is -1 when Server sends no Content-Length header)
//         static uint8_t buff[200] = {0}; // create buffer for read
//         int offset = 0;                 // read all data from server
//         while (https.connected() && (len > 0 || len == -1))
//         {
//           size_t size = client->available(); // get available data size
//           if (size)
//           {
//             int c = client->readBytes(buff, ((size > sizeof(buff)) ? sizeof(buff) : size));
//             memcpy(_buffer + offset, buff, sizeof(uint8_t) * c);
//             offset += c;
//             if (len > 0)
//             {
//               len -= c;
//             }
//           }
//           delay(1);
//         }
//         _bufferSize = offset;
//       }
//     }
//     else
//     {
//       Serial.printf("[HTTPS] GET... failed, error: %s\n", https.errorToString(httpCode).c_str());
//     }
//     https.end();
//   }
//   else
//   {
//     Serial.printf("Unable to connect\n");
//   }
//   return _bufferSize > 0;
// }

bool getString(const char *url, uint8_t *&outbuf, size_t &outlen)
{
  uint8_t _buffer[1024];
  size_t _bufferSize = 0;

  // fetchBuffer(url); // HTTPS获取数据流
  //std::unique_ptr<WiFiClientSecure> client(new WiFiClientSecure);
  //WiFiClient client;

  std::unique_ptr<WiFiClientSecure> client(new WiFiClientSecure);
  //std::unique_ptr<BearSSL::WiFiClientSecure>client(new BearSSL::WiFiClientSecure);
  client->setInsecure(); //不检验
  HTTPClient https;

  if (https.begin(*client, url))
  {
    https.addHeader("Accept-Encoding", "gzip");
    int httpCode = https.GET();
    if (httpCode == HTTP_CODE_OK)
    {
      int len = https.getSize();      // get length of document (is -1 when Server sends no Content-Length header)
      static uint8_t buff[200] = {0}; // create buffer for read
      int offset = 0;                 // read all data from server
      while (https.connected() && (len > 0 || len == -1))
      {
        size_t size = client->available(); // get available data size
        if (size)
        {
          int c = client->readBytes(buff, ((size > sizeof(buff)) ? sizeof(buff) : size));
          memcpy(_buffer + offset, buff, sizeof(uint8_t) * c);
          offset += c;
          if (len > 0)
          {
            len -= c;
          }
        }
        taskYIELD(); // delay(1);
      }
      _bufferSize = offset;
    }
    else
    {
#ifdef Debug
      Serial.printf("[HTTPS] GET... failed, error: %s\n", https.errorToString(httpCode).c_str());
#endif
      https.end();
      return false;
    }
    https.end();
  }
  else
  {
#ifdef Debug
        Serial.printf("Unable to connect\n");
#endif
  }
  if (_bufferSize)
  {
    ArduinoUZlib::decompress(_buffer, _bufferSize, outbuf, outlen); // GZIP解压
    _bufferSize = 0;
    return true;
  }
  return false;
}

String Https_get_Nzip(String url)
{
  uchar num = 0;
  
  //WiFiClient client;
  //BearSSLClient sslClient(client);
  //std::unique_ptr<BearSSL::WiFiClientSecure> client(new BearSSL::WiFiClientSecure);
  std::unique_ptr<WiFiClientSecure> client(new WiFiClientSecure);
  //std::unique_ptr<BearSSL::WiFiClientSecure>client(new BearSSL::WiFiClientSecure);
  client->setInsecure(); // 不进行服务器身份认证
  HTTPClient https;
kai:
  if (https.begin(*client, url)) // HTTPS连接成功
  {
    //  taskYIELD();
    https.addHeader("Accept-Encoding", "gzip");
    int httpCode = https.GET(); // 请求
    if (httpCode > 0)
    {
      if (httpCode == HTTP_CODE_OK || httpCode == HTTP_CODE_MOVED_PERMANENTLY)
      { // 服务器响应
        return https.getString();
#ifdef Debug
        Serial.println("HTTP:\n");
        Serial.println(ESP.getFreeHeap());
#endif
      }
    }
    else
    { // 错误返回
#ifdef Debug
      Serial.println(url);
      Serial.printf("[HTTPS] GET... failed, error: %s\n", https.errorToString(httpCode).c_str());
#endif
      num++;
      if (num < get_frequency_MAX)
      {
        taskYIELD();
        goto kai;
      }
      https.end();
      return "false";
    }
    https.end();
    return "false";
  }
  else
  { // HTTPS连接失败
    num++;
    if (num < get_frequency_MAX)
    {
      taskYIELD();
      goto kai;
    }
    return "false";
  }
  return "false";
}

bool Https_get_Nzip(String url, String &redata)
{
  uchar num = 0;
  //WiFiClient client;
  //BearSSLClient sslClient(client);
  //std::unique_ptr<BearSSL::WiFiClientSecure> client(new BearSSL::WiFiClientSecure);
  std::unique_ptr<WiFiClientSecure> client(new WiFiClientSecure);
  //std::unique_ptr<BearSSL::WiFiClientSecure>client(new BearSSL::WiFiClientSecure);
  client->setInsecure(); // 不进行服务器身份认证
  HTTPClient https;
kai:
  if (https.begin(*client, url)) // HTTPS连接成功
  {
    //  taskYIELD();
    https.addHeader("Accept-Encoding", "gzip");
    int httpCode = https.GET(); // 请求
    if (httpCode > 0)
    {
      if (httpCode == HTTP_CODE_OK || httpCode == HTTP_CODE_MOVED_PERMANENTLY)
      { // 服务器响应
        redata = https.getString();
#ifdef Debug
        Serial.println("HTTP:\n");
        Serial.println(ESP.getFreeHeap());
#endif
        https.end();
        return true;
      }
    }
    else
    { // 错误返回
#ifdef Debug
      Serial.println(url);
      Serial.printf("[HTTPS] GET... failed, error: %s\n", https.errorToString(httpCode).c_str());
#endif
      num++;
      if (num < get_frequency_MAX)
      {
        taskYIELD();
        goto kai;
      }
      https.end();
      return false;
    }
    https.end();
    return false;
  }
  else
  { // HTTPS连接失败
    num++;
    if (num < get_frequency_MAX)
    {
      taskYIELD();
      goto kai;
    }
    return false;
  }
  return false;
}

char *Https_get_zip(String url)
{
  uint8_t *outbuf = NULL;
  char *outbuf1 = (char *)outbuf;
  size_t len = 0;
  if (getString(url.c_str(), outbuf, len) == false)
    return NULL;
#ifdef Debug
  Serial.printf("char\n解压数据: %s\n", outbuf);
#endif
  return outbuf1;
}

bool Https_get_zip(String url, uint8_t *&outbuf)
{
  size_t len = 0;
  bool sa = false;
  sa = getString(url.c_str(), outbuf, len);

#ifdef Debug
  // Serial.printf("解压数据: %s\n", outbuf);
#endif
  return sa;
}

// bool Https_get_zip(String url)
// {
//   size_t len = 0;
//   bool sa = false;
//   sa = getString(url.c_str(), payload_z, len);
// #ifdef Debug
//   // Serial.printf("解压数据: %s\n", outbuf);
// #endif
//   return sa;
// }

// 判断天气信息是否有需要语音播报的
void warn(void)
{
  if (today.temmax >= 29 && yesterday.temmax < 29){ // 昨天温度低于29，今天温度高于29则播报
    //tem_broadcast(today.temmax, today.temmin, 2);
  }

  if ((yesterday.temmin - today.temmin) >= 5 && today.temmin < 15){ // 今天最低气温比昨天低了5摄氏度播报
    //tem_broadcast(today.temmax, today.temmin, 1);
  }

  if (compare(today.weather.c_str(), "雨") == 1){}
    //warn_Rain(); // 播放可能有雨提醒带伞

  if (today.Uv > 2){}
    //Uv_broadcast(today.Uv); // 播放防晒
  yesterday.temmax = today.temmax;
  yesterday.temmin = today.temmin;
  //write_yesteday_wheather(today.temmax, today.temmin);
}
// 查找a里是否有b的内容有则返回1
uchar compare(const char *a, const char *b)
{
  while (*a)
  {
    if (*a == *b && *(a + 1) == *(b + 1) && *(a + 2) == *(b + 2))
      return 1;
    a += 3;
  }
  return 0;
}

/*
// 读取昨天最高和最低温度
void read_yesteday_wheather(void)
{
  uchar kl;
  EEPROM.begin(eepron_capacity); // 声明需要用的容量
  if (EEPROM.read(yday_wher_add) == Year)
    if (EEPROM.read(yday_wher_add + 1) == Month)
      if (EEPROM.read(yday_wher_add + 2) == Day - 1)
      {
        kl = EEPROM.read(yday_wher_add + 3);
        if (kl == 1)
          yesterday.temmax = (-EEPROM.read(yday_wher_add + 4)); // 读取昨天的最高温度
        else if (kl == 0)
          yesterday.temmax = EEPROM.read(yday_wher_add + 4);

        kl = EEPROM.read(yday_wher_add + 5);
        if (kl == 1)
          yesterday.temmin = (-EEPROM.read(yday_wher_add + 6)); // 读取昨天的最低温度
        else if (kl == 0)
          yesterday.temmin = EEPROM.read(yday_wher_add + 6);
      }
}
*/

// bool Cityid_get(String province, String county, String requserKey, String reqLang)
// {
//   std::unique_ptr<BearSSL::WiFiClientSecure> client(new BearSSL::WiFiClientSecure);
//   client->setInsecure(); // 不进行服务器身份认证
//   HTTPClient https;
//   String payload;
//   String url = "https://geoapi.qweather.com/v2/city/lookup?location=" + county + "&adm=" + province + "&lang=" + reqLang + "&key=" + requserKey ;
//   uchar num = 0;
// kai:
//   if (https.begin(*client, url))
//   {                             // HTTPS连接成功
//     int httpCode = https.GET(); // 请求
//     if (httpCode > 0)
//     {
//       if (httpCode == HTTP_CODE_OK || httpCode == HTTP_CODE_MOVED_PERMANENTLY)
//       { // 服务器响应
//         payload = https.getString();
//         parseNowJson(payload, 1); // json解析
// #ifdef Debug
//         Serial.println(url);
//         Serial.println(ESP.getFreeHeap());
//         Serial.println(payload);
// #endif
//         return true;
//       }
//     }
//     else
//     { // 错误返回
// #ifdef Debug
//       Serial.println(url);
//       Serial.println(ESP.getFreeHeap());
//       Serial.printf("[HTTPS] GET... failed, error: %s\n", https.errorToString(httpCode).c_str());
// #endif
//       num++;
//       if (num < get_frequency_MAX)
//       {
//         taskYIELD();
//         goto kai;
//       }
//       return false;
//     }
//     https.end();
//     // Serial.println(url);
//   }
//   else
//   { // HTTPS连接失败
//     num++;
//     if (num < get_frequency_MAX)
//     {
//       taskYIELD();
//       {
//         taskYIELD();
//         goto kai;
//       }
//     }
//     return false;
//   }
//   return true;
// }
#endif
