unsigned int wesign = 0;    // wesign:0，0，多云，少云 | 0，0，0，晴 | 0，冰雹，雪花，雷 | 雨滴，云，月亮，太阳

void weathericon(unsigned int code) //将图标码转换成标志位
{
    switch (code)
    {
    case 100:
        wesign = 0x0101;
        lv_img_set_src(ui_Image99, &ui_img_100_png);
        break; // 晴
    case 101:
        wesign = 0x2005;
        lv_img_set_src(ui_Image99, &ui_img_101_png);
        break; // 多云
    case 102:
        wesign = 0x1005;
        lv_img_set_src(ui_Image99, &ui_img_102_png);
        break; // 少云
    case 103:
        wesign = 0x1005;
        lv_img_set_src(ui_Image99, &ui_img_103_png);
        break; // 晴间多云
    case 104:
        wesign = 0x0004;
        lv_img_set_src(ui_Image99, &ui_img_104_png);
        break; // 阴
    case 150:
        wesign = 0x0102;
        lv_img_set_src(ui_Image99, &ui_img_150_png);
        break; // 夜间晴
    case 153:
        wesign = 0x1006;
        lv_img_set_src(ui_Image99, &ui_img_153_png);
        break; // 夜晴间多云  //wesign:0，0，多云，少云 | 0，0，0，晴 | 0，冰雹，雪花，雷 | 雨滴，云，月亮，太阳
    case 154:
        wesign = 0x0004;
        lv_img_set_src(ui_Image99, &ui_img_154_png);
        break; // 夜间阴
    case 300:
        wesign = 0x200D;
        lv_img_set_src(ui_Image99, &ui_img_300_png);
        break; // 阵雨
    case 301:
        wesign = 0x200D;
        lv_img_set_src(ui_Image99, &ui_img_301_png);
        break; // 强阵雨
    case 302:
        wesign = 0x001C;
        lv_img_set_src(ui_Image99, &ui_img_302_png);
        break; // 雷阵雨
    case 303:
        wesign = 0x001C;
        lv_img_set_src(ui_Image99, &ui_img_303_png);
        break; // 强雷阵雨
    case 304:
        wesign = 0x005C;
        lv_img_set_src(ui_Image99, &ui_img_304_png);
        break; // 雷阵雨伴有冰雹
    case 305:
        wesign = 0x000C;
        lv_img_set_src(ui_Image99, &ui_img_305_png);
        break; // 小雨
    case 306:
        wesign = 0x000C;
        lv_img_set_src(ui_Image99, &ui_img_306_png);
        break; // 中雨
    case 307:
        wesign = 0x000C;
        lv_img_set_src(ui_Image99, &ui_img_307_png);
        break; // 大雨
    case 308:
        wesign = 0x000C;
        lv_img_set_src(ui_Image99, &ui_img_308_png);
        break; // 极端降雨
    case 309:
        wesign = 0x000C;
        lv_img_set_src(ui_Image99, &ui_img_309_png);
        break; // 毛毛雨/细雨
    case 310:
        wesign = 0x000C;
        lv_img_set_src(ui_Image99, &ui_img_310_png);
        break; // 暴雨
    case 311:
        wesign = 0x000C;
        lv_img_set_src(ui_Image99, &ui_img_311_png);
        break; // 大暴雨
    case 312:
        wesign = 0x000C;
        lv_img_set_src(ui_Image99, &ui_img_312_png);
        break; // 特大暴雨
    case 313:
        wesign = 0x000C;
        //lv_img_set_src(ui_Image99, &ui_img_313_png);
        lv_img_set_src(ui_Image99, &ui_img_999_png);
        break; // 冻雨
    case 314:
        wesign = 0x000C;
        lv_img_set_src(ui_Image99, &ui_img_314_png);
        break; // 小到中雨
    case 315:
        wesign = 0x000C;
        lv_img_set_src(ui_Image99, &ui_img_315_png);
        break; // 中到大雨
    case 316:
        wesign = 0x000C;
        lv_img_set_src(ui_Image99, &ui_img_316_png);
        break; // 大到暴雨
    case 317:
        wesign = 0x000C;
        lv_img_set_src(ui_Image99, &ui_img_317_png);
        break; // 暴雨到大暴雨
    case 318:
        wesign = 0x000C;
        lv_img_set_src(ui_Image99, &ui_img_318_png);
        break; // 大暴雨到特大暴雨
    case 399:
        wesign = 0x000C;
        lv_img_set_src(ui_Image99, &ui_img_399_png);
        break; // 雨
    case 350:
        wesign = 0x200E;
        //lv_img_set_src(ui_Image99, &ui_img_350_png);
        lv_img_set_src(ui_Image99, &ui_img_999_png);
        break; // 夜间阵雨
    case 351:
        wesign = 0x200E;
        //lv_img_set_src(ui_Image99, &ui_img_351_png);
        lv_img_set_src(ui_Image99, &ui_img_999_png);
        break; // 夜间强阵雨
    case 400:
        wesign = 0x0024;
        lv_img_set_src(ui_Image99, &ui_img_400_png);
        break; // 小雪
    case 401:
        wesign = 0x0024;
        lv_img_set_src(ui_Image99, &ui_img_401_png);
        break; // 中雪
    case 402:
        wesign = 0x0024;
        lv_img_set_src(ui_Image99, &ui_img_402_png);
        break; // 大雪
    case 403:
        wesign = 0x0024;
        lv_img_set_src(ui_Image99, &ui_img_403_png);
        break; // 暴雪
    case 404:
        wesign = 0x00C4;
        lv_img_set_src(ui_Image99, &ui_img_404_png);
        break; // 雨夹雪
    case 405:
        wesign = 0x00C4;
        lv_img_set_src(ui_Image99, &ui_img_405_png);
        break; // 雨雪天气
    case 406:
        wesign = 0x00C4;
        //lv_img_set_src(ui_Image99, &ui_img_406_png);
        lv_img_set_src(ui_Image99, &ui_img_999_png);
        break; // 阵雨夹雪
    case 407:
        wesign = 0x0024;
        //lv_img_set_src(ui_Image99, &ui_img_407_png);
        lv_img_set_src(ui_Image99, &ui_img_999_png);
        break; // 阵雪
    case 408:
        wesign = 0x0024;
        lv_img_set_src(ui_Image99, &ui_img_408_png);
        break; // 小到中雪
    case 409:
        wesign = 0x0024;
        lv_img_set_src(ui_Image99, &ui_img_409_png);
        break; // 中到大雪
    case 410:
        wesign = 0x0024;
        lv_img_set_src(ui_Image99, &ui_img_410_png);
        break; // 大到暴雪
    case 499:
        wesign = 0x0024;
        lv_img_set_src(ui_Image99, &ui_img_499_png);
        break; // 雪
    case 456:
        wesign = 0x0024;
        //lv_img_set_src(ui_Image99, &ui_img_456_png);
        lv_img_set_src(ui_Image99, &ui_img_999_png);
        break; // 夜间阵雨夹雪
    case 457:
        wesign = 0x0024;
        //lv_img_set_src(ui_Image99, &ui_img_457_png);
        lv_img_set_src(ui_Image99, &ui_img_999_png);
        break; // 夜间阵雪
    case 500:
        wesign = 0x0004;
        lv_img_set_src(ui_Image99, &ui_img_500_png);
        break; // 薄雾
    case 501:
        wesign = 0x0004;
        lv_img_set_src(ui_Image99, &ui_img_501_png);
        break; // 雾
    case 502:
        wesign = 0x0004;
        lv_img_set_src(ui_Image99, &ui_img_502_png);
        break; // 霾
    case 503:
        wesign = 0x0004;
        lv_img_set_src(ui_Image99, &ui_img_999_png);
        //lv_img_set_src(ui_Image99, &ui_img_503_png);
        break; // 扬沙
    case 504:
        wesign = 0x0004;
        lv_img_set_src(ui_Image99, &ui_img_999_png);
        //lv_img_set_src(ui_Image99, &ui_img_504_png);
        break; // 浮尘
    case 507:
        wesign = 0x0004;
        lv_img_set_src(ui_Image99, &ui_img_999_png);
        //lv_img_set_src(ui_Image99, &ui_img_507_png);
        break; // 沙尘暴
    case 508:
        wesign = 0x0004;
        lv_img_set_src(ui_Image99, &ui_img_999_png);
        //lv_img_set_src(ui_Image99, &ui_img_508_png);
        break; // 强沙尘暴
    case 509:
        wesign = 0x0004;
        //lv_img_set_src(ui_Image99, &ui_img_509_png);
        lv_img_set_src(ui_Image99, &ui_img_999_png);
        break; // 浓雾
    case 510:
        wesign = 0x0004;
        lv_img_set_src(ui_Image99, &ui_img_999_png);
        //lv_img_set_src(ui_Image99, &ui_img_510_png);
        break; // 强浓雾
    case 511:
        wesign = 0x0004;
        lv_img_set_src(ui_Image99, &ui_img_511_png);
        break; // 中度霾
    case 512:
        wesign = 0x0004;
        lv_img_set_src(ui_Image99, &ui_img_512_png);
        break; // 重度霾
    case 513:
        wesign = 0x0004;
        lv_img_set_src(ui_Image99, &ui_img_999_png);
        //lv_img_set_src(ui_Image99, &ui_img_513_png);
        break; // 严重霾
    case 514:
        wesign = 0x0004;
        lv_img_set_src(ui_Image99, &ui_img_514_png);
        break; // 大雾
    case 515:
        wesign = 0x0004;
        lv_img_set_src(ui_Image99, &ui_img_999_png);
        //lv_img_set_src(ui_Image99, &ui_img_515_png);
        break; // 特强浓雾
    case 999:
        wesign = 0x0004;
        lv_img_set_src(ui_Image99, &ui_img_999_png);
        break; // 未知
    default:
        wesign = 0x0004;
        lv_img_set_src(ui_Image99, &ui_img_999_png);
        break;
    }
}
