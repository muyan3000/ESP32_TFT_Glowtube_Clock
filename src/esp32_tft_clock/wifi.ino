
void SmartConfig()
{
  int delay_times = 0;
  // WiFi.mode(WIFI_AP_STA);
  WiFi.mode(WIFI_STA);
  Serial.println("\r\n wait for smartconfig....");
  WiFi.beginSmartConfig();
  while (1)
  {
    if (delay_times > 60)
    {
      ESP.restart();
    }
    Serial.print(".");
    delay(500);
    if (WiFi.smartConfigDone())
    {
      Serial.println("SmartConfig Success");
      // Serial.printf("SSID:%s\r\n", WiFi.SSID().c_str());
      // Serial.printf("PSW:%s\r\n", WiFi.psk().c_str());
      break;
    }
    delay_times++;
  }
}

bool AutoWifiConfig()
{

  // 设置固定IP地址
  // IPAddress staticIP(192, 168, 5, 24); //ESP static ip
  // IPAddress gateway(192, 168, 5, 1);   //IP Address of your WiFi Router (Gateway)
  // IPAddress subnet(255, 255, 255, 0);  //Subnet mask
  // IPAddress dns(192, 168, 5, 1);  //DNS
  // WiFi.config(staticIP, gateway, subnet, dns);
  WiFi.begin(ssid, password); // Wifi接入到网络
  // WiFi.begin(); //Wifi接入到网络
  // 如果觉得时间太长可改
  for (int i = 0; i < 20; i++)
  {
    int wstatus = WiFi.status();
    if (wstatus == WL_CONNECTED)
    {
      Serial.println("WIFI AutoConfig Success");
      Serial.print("LocalIP:");
      Serial.print(WiFi.localIP());
      Serial.print(" ,GateIP:");
      Serial.println(WiFi.gatewayIP());

      //******  ntp时间服务初始化 ******
      timeClient.begin();


      // 以下是启动OTA，可以通过WiFi刷新固件
      ArduinoOTA.setHostname("ESP32-Clock");
      // No authentication by default
      // ArduinoOTA.setPassword("admin");

      // Password can be set with it's md5 value as well
      // MD5(admin) = 21232f297a57a5a743894a0e4a801fc3
      ArduinoOTA.setPasswordHash("21232f297a57a5a743894a0e4a801fc3");
      ArduinoOTA.onStart([]()
                         {
    String type;
    if (ArduinoOTA.getCommand() == U_FLASH) {
      type = "sketch";
    } else { // U_SPIFFS
      type = "filesystem";
    }

    // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
    Serial.println("Start updating " + type); });
      ArduinoOTA.onEnd([]()
                       { Serial.println("\nEnd"); });
      ArduinoOTA.onProgress([](unsigned int progress, unsigned int total)
                            { Serial.printf("Progress: %u%%\r", (progress / (total / 100))); });
      ArduinoOTA.onError([](ota_error_t error)
                         {
    Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) {
      Serial.println("Auth Failed");
    } else if (error == OTA_BEGIN_ERROR) {
      Serial.println("Begin Failed");
    } else if (error == OTA_CONNECT_ERROR) {
      Serial.println("Connect Failed");
    } else if (error == OTA_RECEIVE_ERROR) {
      Serial.println("Receive Failed");
    } else if (error == OTA_END_ERROR) {
      Serial.println("End Failed");
    } });
      ArduinoOTA.begin();

      return true;
    }
    else
    {
      Serial.print("WIFI AutoConfig Waiting......");
      Serial.println(wstatus);
      delay(1000);
    }
    
    taskYIELD();
  }
  Serial.println("WIFI AutoConfig Faild!");
  return false;
}
